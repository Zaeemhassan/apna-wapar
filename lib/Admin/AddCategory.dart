import 'package:apna_beepar/Admin/AdminHomePage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io';
import 'package:flutter/widgets.dart';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:path/path.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class CreateAnimalCategory extends StatefulWidget {
  @override
  _CreateAnimalCategoryState createState() => new _CreateAnimalCategoryState();
}

class _CreateAnimalCategoryState extends State<CreateAnimalCategory> {

  bool isVisible = false;
  var isLoading = true;
  File _image;
  var categoryImageurl;
  var userId;
  var animalCategory;

  Future getImage() async {
    // ignore: deprecated_member_use
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
    String fileName = basename(_image.path);
    StorageReference firebaseStorageRef = FirebaseStorage.instance
        .ref()
        .child('/assets/categoryimage/$fileName');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    categoryImageurl = await firebaseStorageRef.getDownloadURL() as String;
    setState(() {

    });
  }

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  final FirebaseAuth auth = FirebaseAuth.instance;
  Future _onPressed() async {
    Firestore.instance.collection("Animal Category").add({
      "Animal Category": {
        "AnimalName": animalCategory,
        "AnimalImage": categoryImageurl
      },
    }).then((value) {
      print(value.documentID);
      print(value);
    });
//
  }


  @override
  void initState() {
    super.initState();

  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        home:Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[500],
        title: Text(
          'Create Animal Category',
          style: TextStyle(fontSize: 17, fontFamily: 'Righteous',color: Colors.white),
        ),
        bottom: PreferredSize(
            child: Container(
              color: Colors.black87,
              height: 8.0,
            ),
            preferredSize: Size.fromHeight(8.0)),
      ),
      floatingActionButton: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          FloatingActionButton.extended(
            heroTag: null,
            onPressed: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                setState(() {
                  isLoading = true;
                });

                _onPressed();
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => AdminHomePage()));
              }


            },
            icon: Icon(
              Icons.add_circle,
              color: Colors.white,
            ),
            label: Text(
              'Add',
              style: TextStyle(fontFamily: 'Righteous', color: Colors.white),
            ),
            backgroundColor: Colors.lightGreen[600],
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body:Center(
    child:
     Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 30,
              ),
              Container(
                height: 200,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Align(
                        alignment: Alignment.center,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: (_image != null)
                              ? Image.file(
                            _image,
                            fit: BoxFit.fill,
                            width: 200,
                            height: 150,
                          )
                              : Image.asset(
                            "assets/images/loading.gif",
                            fit: BoxFit.fill,
                          ),
                        )),
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                      child: IconButton(
                        icon: Icon(
                          FontAwesomeIcons.camera,
                          size: 30.0,
                        ),
                        onPressed: () {
                          getImage();
//                                    uploadPic(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                alignment: Alignment.topCenter,
                margin: EdgeInsets.all(10),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: "Animal Name",
                    hintText: "Animal Name",
                    prefixIcon: Icon(
                      Icons.title,
                      color: Colors.lightGreen[400],
                    ),
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                      borderSide: new BorderSide(),
                    ),
                    //fillColor: Colors.green
                  ),
                  onSaved: (String value) {
                    animalCategory = value;
                  },
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Enter animal name';
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(height: 10.0),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
        ) ));
  }

}
