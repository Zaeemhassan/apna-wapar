import 'dart:io';

import 'package:apna_beepar/Admin/AddCategory.dart';
import 'package:apna_beepar/Admin/ViewAnimalCategory.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:apna_beepar/Admin/AllUsersProfile/ViewAllUsers.dart';
import 'package:apna_beepar/Admin/AllAnimalPosts/ViewAllPosts.dart';

class AdminHomePage extends StatefulWidget {
  @override
  _AdminHomePageState createState() => _AdminHomePageState();
}

class _AdminHomePageState extends State<AdminHomePage> {
  int _currentIndex = 0;
  final List<Widget> _children = [

    CreateAnimalCategory(),
    ViewCategory(),
    ViewAllAnimalList(),
    AllUsers(),


  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

//  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: _children[_currentIndex], // new
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.shifting,
        selectedItemColor: CupertinoColors.activeGreen,
        unselectedItemColor: CupertinoColors.systemGrey,
        showSelectedLabels: true,
        showUnselectedLabels: false,
        onTap: onTabTapped, // new
        currentIndex: _currentIndex, // new
        items: [
          new BottomNavigationBarItem(
              icon: Icon(
                Icons.add_circle,
                // color: Colors.blueGrey[300],
              ),
              title: Text("Add Animal Category")),

          new BottomNavigationBarItem(
              icon: Icon(
                Icons.view_list,
                // color: Colors.blueGrey[300],
              ),
              title: Text("View Category")),
          new BottomNavigationBarItem(
              icon: Icon(
                Icons.category,
                // color: Colors.blueGrey[300],
              ),
              title: Text("All Animal Posts")),

          new BottomNavigationBarItem(
              icon: Icon(
                Icons.account_circle,
                // color: Colors.blueGrey[300],
              ),
              title: Text("All Users")),
        ],
      ),
    );
  }
}
