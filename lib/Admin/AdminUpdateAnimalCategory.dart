import 'package:apna_beepar/Admin/AdminHomePage.dart';
import 'package:apna_beepar/Admin/ViewAnimalCategory.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class UpdateCategory extends StatefulWidget {
  final String updateAnimalName;
  var updateAnimalImage;
  var documentId;
  @override
  _UpdateCategoryState createState() => _UpdateCategoryState();
  UpdateCategory({
    Key key,
    @required this.updateAnimalName,
    @required this.updateAnimalImage,
    @required this.documentId,
  }) : super(key: key);
}

class _UpdateCategoryState extends State<UpdateCategory> {
  bool isVisible = false;
  var isLoading = true;
  File _image;
  var menuImageurl;
  var userId;
  var updateMenuName;
  var updateMenuImage;
//  final catTitle = TextEditingController();
  final catImage = TextEditingController();


  Future getImage() async {
    // ignore: deprecated_member_use
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
    String fileName = basename(_image.path);
    StorageReference firebaseStorageRef = FirebaseStorage.instance
        .ref()
        .child('assets/categoryimage/$fileName');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    menuImageurl = await firebaseStorageRef.getDownloadURL() as String;
    setState(() {
      print("Profile Picture uploaded");
    });
  }

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  final FirebaseAuth auth = FirebaseAuth.instance;

  Future updateMenu() async {
    Firestore.instance
        .collection("Animal Category")
        .document(widget.documentId)
        .updateData({
      "Animal Category": {
        "AnimalImage": (_image != null) ? menuImageurl : widget.updateAnimalImage,
        "AnimalName": updateMenuName,

      },
    }).then((value) {});
//
  }

  @override
  void initState() {
    super.initState();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[500],
        title: Text(
          'Update Animal Category',
          style: TextStyle(
              fontSize: 17, fontFamily: 'Righteous', color: Colors.white),
        ),
        bottom: PreferredSize(
            child: Container(
              color: Colors.black87,
              height: 8.0,
            ),
            preferredSize: Size.fromHeight(8.0)),
      ),
      floatingActionButton: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          FloatingActionButton.extended(
            heroTag: null,
            onPressed: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                setState(() {
                  isLoading = true;
                });

                updateMenu();
              }

              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => ViewCategory()));
            },
            icon: Icon(
              Icons.update,
              color: Colors.white,
            ),
            label: Text(
              'Update Category',
              style: TextStyle(fontFamily: 'Righteous', color: Colors.white),
            ),
            backgroundColor: Colors.lightGreen[600],
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 30,
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 200,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Align(
                        alignment: Alignment.center,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: (_image != null)
                              ? Image.file(
                            _image,
                            fit: BoxFit.fill,
                          )
                              : FadeInImage(
                            image:
                            NetworkImage(
                              widget.updateAnimalImage.toString(),),
                            fit: BoxFit.fill,
                            width: 200,
                            height: 150,
                            placeholder: AssetImage(
                                'assets/images/loading.gif'),
                          ),
                        )),
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                      child: IconButton(
                        icon: Icon(
                          FontAwesomeIcons.camera,
                          size: 30.0,
                        ),
                        onPressed: () {
                          getImage();
//                                    uploadPic(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                alignment: Alignment.topCenter,
                margin: EdgeInsets.all(10),
                child: TextFormField(
                  initialValue: " ${widget.updateAnimalName}",
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: "Update Animal Name",
                    hintText: "abc",
                    prefixIcon: Icon(
                      Icons.title,
                      color: Colors.lightGreen[400],
                    ),
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                      borderSide: new BorderSide(),
                    ),
                    //fillColor: Colors.green
                  ),
                  onSaved: (String value) {
                    updateMenuName = value;
                  },
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Enter your Animal Name ';
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(height: 10.0),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );

    // This trailing comma makes auto-formatting nicer for build methods.
  }
}
