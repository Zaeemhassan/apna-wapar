import 'dart:io';

import 'package:apna_beepar/BuyPets/ViewBuyAnimalListDetails.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:video_player/video_player.dart';
import 'package:apna_beepar/UserHomePage/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geolocator/geolocator.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:video_player/video_player.dart';
import 'package:apna_beepar/Admin/AllAnimalPosts/PostDetail.dart';

class ViewAllAnimalList extends StatefulWidget {


  @override
  _ViewAllAnimalListState createState() => new _ViewAllAnimalListState();

}

class _ViewAllAnimalListState extends State<ViewAllAnimalList> {
  bool isLoading = false;
  var id;
  String radioButtonItem = translator.translate('All');
  String radioButton = translator.translate('Date');
  int typeId = 1;
  int allId = 1;
  List itemsData = [];
  List animalName = [];
  var animalImage1 = [];
  var animalImage2 = [];
  var animalImage3 = [];
  var animalPrice = [];
  var animalSellerLocation = [];
  var animalCategory = [];
  var pregnancyCount = [];
  var animalGender = [];
  var pregnancyStatus = [];
  var animalBreed = [];
  var animalDescription = [];
  var animalBuyerName = [];
  var animalBuyerPhone = [];
  var animalUserId = [];
  var animalVideo = [];
  List document = [];
  var allImages = [];
  var sellingData;
  var allAnimalsData = [];
  var deleteCategoryData;
  VideoPlayerController _controller;
  bool dataCheck = false;
  String _currentAddress;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  TextEditingController searchController = new TextEditingController();
  String filter;
  Future getAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")

        .orderBy("SellAnimalList.DateTime", descending: true)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
      });
      setState(() {
        isLoading = false;
      });
    });
  }

  Future getTodayAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .where('SellAnimalList.DateTime',
        isGreaterThan: DateTime.now().subtract(Duration(days: 1)))
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
      });
      print("Animal video $animalVideo");
      setState(() {
        isLoading = false;
      });
    });
  }

  Future get48HourAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .where('SellAnimalList.DateTime',
        isGreaterThan: DateTime.now().subtract(Duration(days: 2)))
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
      });
      setState(() {
        isLoading = false;
      });
    });
  }

  Future getPriceLowToHighAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .orderBy("SellAnimalList.AnimalPrice", descending: false)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
      });
      setState(() {
        isLoading = false;
      });
    });
  }

  Future getDateWiseAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .orderBy("SellAnimalList.DateTime", descending: true)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
      });
      setState(() {
        isLoading = false;
      });
    });
  }

  Future getPriceHighToLowAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .orderBy("SellAnimalList.AnimalPrice", descending: true)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
      });
      setState(() {
        isLoading = false;
      });
    });
  }

  _getCurrentLocation() {
    setState(() {
      isLoading = true;
    });
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
      setState(() {
        isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress = "${place.locality}, ${place.country}";
      });
      print('address $_currentAddress');
    } catch (e) {
      print(e);
    }
  }

  var userId;
  var name;
  Future getUserProfile() async {
    setState(() {
      isLoading = true;
    });
    var firebaseUser = await FirebaseAuth.instance.currentUser();
    userId = firebaseUser.uid;
    Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .get()
        .then((value) {
      print(value.data);
      var id = value.documentID;
      var user = value.data;
      name = user["name"];
      setState(() {
        isLoading = false;
      });
    });
  }
  BannerAd _bannerAd;
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo();
  BannerAd createBannerAd() {
    return new BannerAd(
        adUnitId: "ca-app-pub-9179231796573696/8084181025",
        size: AdSize.banner,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("Banner event : $event");
        });
  }
  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
        adUnitId: "ca-app-pub-9179231796573696/5951474729",
        //Change Interstitial AdUnitId with Admob ID
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("IntersttialAd $event");
        });
  }
  final FirebaseAuth auth = FirebaseAuth.instance;
  var currentDateTime;

  Future<bool> _onWillPop() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding:
            EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0, bottom: 0.0),
            children: <Widget>[
              Container(
                color: Colors.red,
                margin: EdgeInsets.all(0.0),
                padding: EdgeInsets.only(bottom: 10.0, top: 10.0),
                height: 100.0,
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.exit_to_app,
                        size: 30.0,
                        color: Colors.white,
                      ),
                      margin: EdgeInsets.only(bottom: 10.0),
                    ),
                    Text(
                      'Exit app',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Are you sure to exit app?',
                      style: TextStyle(color: Colors.white, fontSize: 14.0),
                    ),
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.cancel,
                        color: Colors.black,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'CANCEL',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: ()=> exit(0),

                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.check_circle,
                        color: Colors.black,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'YES',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          );
        }
    )??
        false;
  }
  @override
  void initState() {
    FirebaseAdMob.instance
        .initialize(appId: "ca-app-pub-9179231796573696~8878581468");
    _bannerAd = createBannerAd()
      ..load()
      ..show();
    _getCurrentLocation();
    super.initState();
    getAnimalSellingData();
    getUserProfile();
    currentDateTime = DateTime.now();
    searchController.addListener(() {
      setState(() {
        filter = searchController.text;
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bannerAd?.dispose();
    searchController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onWillPop,
        child:Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightGreen[500],
          title: Text(
            "All Posts",
            style: TextStyle(fontFamily: 'Righteous',fontSize: 17),
          ),
          bottom: PreferredSize(
              child: Container(
                color: Colors.black87,
                height: 8.0,
              ),
              preferredSize: Size.fromHeight(8.0)),

        ),
        backgroundColor: Colors.white,
        body: isLoading
            ? SpinKitDoubleBounce(
          color: Colors.greenAccent[700],
        )
            : Column(children: <Widget>[
          Container(
            child: InkWell(
              onTap: () async {
                // Navigator.push(
                //     context,
                //     MaterialPageRoute(
                //         builder: (context) => ViewPersonalSellingList()));
              },
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                color: Colors.white,
                elevation: 1,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                        title: Row(
                          children: [
                            Expanded(
                              child: SizedBox(
                                  height: 60,
                                  width: 70,
                                  child: InkWell(
                                      onTap: () {
                                        showAll();
                                      },
                                      child: Card(
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(Icons.arrow_drop_down_sharp),
                                              Text(
                                                '$radioButtonItem',
                                                style: TextStyle(
                                                    fontFamily: 'Righteous',
                                                    fontSize: 17),
                                              ),
                                            ],
                                          )))),
                            ),
                            Expanded(
                                child: SizedBox(
                                    height: 60,
                                    width: 70,
                                    child: InkWell(
                                        onTap: () {
                                          showSort();
                                        },
                                        child: Card(
                                            child: Center(
                                              child: Text(
                                                translator.translate('Sort'),
                                                style: TextStyle(
                                                    fontFamily: 'Righteous',
                                                    fontSize: 17),
                                              ),
                                            ))))),
                          ],
                        )),
                  ],
                ),
              ),
            ),
          ),

          SizedBox(
            height: 10,
          ),
          Expanded(
            child: ListView(children: [
              Container(
                  margin: const EdgeInsets.all(10),
                  child: GridView.builder(
                      shrinkWrap: true,
                      physics: BouncingScrollPhysics(),
                      itemCount: animalName?.length ?? 0,
                      gridDelegate:
                      SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 1),
                      itemBuilder: (context, index) {
                        return InkWell(
                          splashColor: Colors.green,
                          onTap: () {
                            allImages.clear();
                            allImages.add(animalImage1[index]);
                            allImages.add(animalImage2[index]);
                            allImages.add(animalImage3[index]);
                            Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) =>
                                      ViewPostDetails(
                                        animalNameDetail:
                                        animalName[index],
                                        animalLocationDetail:
                                        animalSellerLocation[
                                        index],
                                        animalDescriptionDetail:
                                        animalDescription[
                                        index],
                                        animalGenderDetail:
                                        animalGender[index],
                                        animalAllImages: allImages,
                                        animalPregnancyCount:
                                        pregnancyCount[index],
                                        animalPregnancyStatus:
                                        pregnancyStatus[index],
                                        animalPriceDetail:
                                        animalPrice[index],
                                        animalBreedDetail:
                                        animalBreed[index],
                                        animalBuyerNameDetail:
                                        animalBuyerName[index],
                                        animalPhoneNumberDetail:
                                        animalBuyerPhone[index],
                                        animalSellerUserId:
                                        animalUserId[index],
                                        animalVideoDetail:
                                        animalVideo[index],
                                      )),
                            );
                          },
                          child: Card(
                            elevation: 8,
                            semanticContainer: true,
                            color: Colors.white,
                            child: Column(children: [
                              ClipRRect(
                                borderRadius:
                                BorderRadius.circular(15.0),
                                child: Image.network(
                                  animalImage1[index].toString(),
                                  height: 200.0,
                                  width: 400.0,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              Container(
                                  margin: const EdgeInsets.fromLTRB(
                                      10, 5, 10, 0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          translator.translate(
                                              "Animal Category: "),
                                          style: TextStyle(
                                              fontFamily:
                                              "Righteous",
                                              fontSize: 17),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Text(
                                          animalName[index]
                                              .toString(),
                                          style: TextStyle(
                                              fontFamily:
                                              "Righteous",
                                              fontSize: 17),
                                        ),
                                      )
                                    ],
                                  )),
                              Container(
                                  margin: const EdgeInsets.fromLTRB(
                                      10, 5, 10, 0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          translator
                                              .translate("Price: "),
                                          style: TextStyle(
                                              fontFamily:
                                              "Righteous",
                                              fontSize: 17),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Text(
                                          animalPrice[index]
                                              .toString() +
                                              translator
                                                  .translate("Rs."),
                                          style: TextStyle(
                                              fontFamily:
                                              "Righteous",
                                              fontSize: 17),
                                        ),
                                      )
                                    ],
                                  )),

                            ]),
                          ),
                        );

                      })),
              SizedBox(
                height: 20,
              )
            ]),
          )
        ])));
  }

  void showAll() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              width: 260.0,
              height: 160.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(60.0)),
              ),
              child: Column(children: [
                Row(children: [
                  Radio(
                    value: 1,
                    groupValue: id,
                    onChanged: (val) {
                      setState(() {
                        radioButtonItem = 'All';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        getAnimalSellingData();
                        Navigator.of(context).pop();

                        id = 1;
                      });
                    },
                  ),
                  Text(
                    translator.translate('All'),
                    style: new TextStyle(fontSize: 14.0),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 2,
                    groupValue: id,
                    onChanged: (val) {
                      setState(() {
                        radioButtonItem = 'Today';
                        getTodayAnimalSellingData();
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        Navigator.of(context).pop();
                        id = 2;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Today'),
                    style: new TextStyle(
                      fontSize: 14.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 3,
                    groupValue: id,
                    onChanged: (val) {
                      setState(() {
                        radioButtonItem = 'In 48 hours';
                        get48HourAnimalSellingData();
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        pregnancyStatus.clear();
                        animalVideo.clear();
                        pregnancyCount.clear();
                        Navigator.of(context).pop();
                        id = 3;
                      });
                    },
                  ),
                  Text(
                    translator.translate('In 48 hours'),
                    style: new TextStyle(
                      fontSize: 14.0,
                    ),
                  ),
                ]),
              ]),
            ),
            backgroundColor: Colors.white,
            actions: [
              FlatButton(
                child: Text(translator.translate('Cancel')),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });

//
  }

  void showSort() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              width: 260.0,
              height: 150.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(60.0)),
              ),
              child: Column(children: [
                Row(children: [
                  Radio(
                    value: 1,
                    groupValue: allId,
                    onChanged: (val) {
                      setState(() {
                        radioButton = 'Date';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        getDateWiseAnimalSellingData();
                        Navigator.of(context).pop();
                        allId = 1;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Date'),
                    style: new TextStyle(fontSize: 14.0),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 2,
                    groupValue: allId,
                    onChanged: (val) {
                      setState(() {
                        radioButton = 'Price low to high';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        getPriceLowToHighAnimalSellingData();
                        Navigator.of(context).pop();
                        allId = 2;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Price low to high'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 3,
                    groupValue: allId,
                    onChanged: (val) {
                      setState(() {
                        radioButton = 'Price high to low';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalVideo.clear();
                        animalBreed.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        getPriceHighToLowAnimalSellingData();
                        Navigator.of(context).pop();
                        allId = 3;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Price high to low'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
              ]),
            ),
            backgroundColor: Colors.white,
            actions: [
              FlatButton(
                child: Text(translator.translate('Cancel')),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });

//
  }
}
