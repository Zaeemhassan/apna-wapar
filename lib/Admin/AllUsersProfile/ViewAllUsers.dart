import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';


class AllUsers extends StatefulWidget {
  @override
  _AllUsersState createState() => new _AllUsersState();
}

class _AllUsersState extends State<AllUsers> {
  var allUserName = [];
  var allUserAddress = [];
  var allUserPhoneNumber = [];
  List allUserRating = [];
 List allUserCount = [];
  var id;
  List document = [];
  bool isLoading = false;
  final FirebaseAuth auth = FirebaseAuth.instance;
  bool dataCheck = false;
  TextEditingController searchController = new TextEditingController();
  String filter;

  Future getAllUsers() async {
    setState(() {
      isLoading = true;
    });
    Firestore.instance
        .collection("users")
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);

        var allUserDate = (result.data);
        print("data ${allUserDate}");
        allUserName.add(allUserDate["name"]);
        allUserAddress.add(allUserDate["Address"]);
        allUserPhoneNumber.add(allUserDate["PhoneNumber"]);
        allUserRating.add((allUserDate["rating"]));
        allUserCount.add(allUserDate["count"]);
        if (allUserDate.isNotEmpty) {
          setState(() {
            dataCheck = true;
          });
        }
      });
       for(int i = 0; i<allUserRating.length; i++){

         totalRatingWithOnlyOneDecimal.add(allUserRating[i] / (allUserCount[i]));
         totalRating.add(totalRatingWithOnlyOneDecimal[i].toStringAsFixed(1));
       }
       print("rating $totalRatingWithOnlyOneDecimal");
      setState(() {
        isLoading = false;
      });
    });
  }
List<double> totalRatingWithOnlyOneDecimal = [];
  List totalRating = [];
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<bool> _onWillPop() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding:
            EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0, bottom: 0.0),
            children: <Widget>[
              Container(
                color: Colors.red,
                margin: EdgeInsets.all(0.0),
                padding: EdgeInsets.only(bottom: 10.0, top: 10.0),
                height: 100.0,
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.exit_to_app,
                        size: 30.0,
                        color: Colors.white,
                      ),
                      margin: EdgeInsets.only(bottom: 10.0),
                    ),
                    Text(
                      'Exit app',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Are you sure to exit app?',
                      style: TextStyle(color: Colors.white, fontSize: 14.0),
                    ),
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.cancel,
                        color: Colors.black,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'CANCEL',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: ()=> exit(0),

                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.check_circle,
                        color: Colors.black,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'YES',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          );
        }
    )??
        false;
  }
  @override
  void initState() {
    super.initState();
  getAllUsers();
    searchController.addListener(() {
      setState(() {
        filter = searchController.text;
      });
    });
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    searchController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle defaultStyle =
    TextStyle(color: Colors.black, fontSize: 13.0, fontFamily: 'Righteous');
    return WillPopScope(
        onWillPop: _onWillPop,
        child:Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.lightGreen[500],
              title: Text(
                'All Users Profile',
                style: TextStyle(fontSize: 17, fontFamily: 'Righteous',color: Colors.white),
              ),
              bottom: PreferredSize(
                  child: Container(
                    color: Colors.black87,
                    height: 8.0,
                  ),
                  preferredSize: Size.fromHeight(8.0)),
            ),
            body: isLoading
                ? SpinKitDoubleBounce(
              color: Colors.greenAccent[700],
            )
                : Center(
              child: ListView(
                  children: dataCheck
                      ? <Widget>[
                    Container(
                      margin: const EdgeInsets.fromLTRB(100, 10, 10, 10),
                      child:Card(
                        child:TextField(
                          controller: searchController,
                          decoration: InputDecoration(
                            labelText: "Search",
                            hintText: "Search",
                            prefixIcon: Icon(
                              Icons.search,
                              color: Colors.green[400],
                            ),
                            fillColor: Colors.white,
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(0.0),
                              borderSide: new BorderSide(),
                            ),
                            //fillColor: Colors.green
                          ),
                          style: TextStyle(
                              color: Colors.black, fontSize: 16.0),
                        ),
                      ),
                    ),

                    Container(
                      margin: const EdgeInsets.all(10),
                      child:
                      ListView.builder(
                          shrinkWrap: true,
                          physics: BouncingScrollPhysics(),
                          itemCount: allUserPhoneNumber.length,
                          itemBuilder: (context, index) {
                            return filter == null || filter == ""
                                ?
                            Card(
                              elevation: 2,
                              color: Colors.lightGreen[100],
                              child: SizedBox(
                                height: 360,
                                child: Column(children: [
                                  Container(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(
                                            10.0),
                                      ),
                                      color: Colors.white,
                                      child: Column(
                                        mainAxisSize:
                                        MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                              title: RichText(
                                                text: TextSpan(
                                                  style: defaultStyle,
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text:
                                                        'User Name :  ',
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .bold,
                                                            fontSize:
                                                            14)),
                                                    TextSpan(
                                                        text: allUserName[index]),
                                                  ],
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(
                                            10.0),
                                      ),
                                      color: Colors.white,
                                      child: Column(
                                        mainAxisSize:
                                        MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                              title: RichText(
                                                text: TextSpan(
                                                  style: defaultStyle,
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text:
                                                        'User PhoneNumber :  ',
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .bold,
                                                            fontSize:
                                                            14)),
                                                    TextSpan(
                                                        text: allUserPhoneNumber[index]),
                                                  ],
                                                ),
                                              )),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(
                                            10.0),
                                      ),
                                      color: Colors.white,
                                      child: Column(

                                        mainAxisSize:
                                        MainAxisSize.min,
                                        children: <Widget>[

                                          ListTile(
                                              title: RichText(
                                                text: TextSpan(
                                                  style: defaultStyle,
                                                  children: <TextSpan>[

                                                    TextSpan(
                                                        text:
                                                        'User Address:  ',
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .bold,
                                                            fontSize:
                                                            14)),
                                                    TextSpan(
                                                        text: allUserAddress[index]),
                                                  ],
                                                ),
                                              )),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(
                                            10.0),
                                      ),
                                      color: Colors.white,
                                      child: Column(
                                        mainAxisSize:
                                        MainAxisSize.min,
                                        children: <Widget>[

                                          ListTile(
                                              title:Row(
                                        children:[
                                        RichText(
                                                text: TextSpan(
                                                  style: defaultStyle,
                                                  children:
                                                  <TextSpan>[
                                                    TextSpan(
                                                        text:
                                                        'Rating :    ',
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .bold,
                                                            fontSize:
                                                            14)),
                                                    TextSpan(
                                                        text: totalRating[index]),
                                                  ],
                                                ),
                                              ),
                                          Icon(Icons.star,color: Colors.yellow[700],)
                                  ])),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(
                                            10.0),
                                      ),
                                      color: Colors.white,
                                      child: Column(
                                        children: <Widget>[
                                          ListTile(
                                              title: RichText(
                                                text: TextSpan(
                                                  style: defaultStyle,
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text:
                                                        'Total Rating Count :  ',
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .bold,
                                                            fontSize:
                                                            14)),
                                                    TextSpan(
                                                        text: allUserCount[index].toString()
                                                           ),
                                                  ],
                                                ),
                                              )),
                                        ],
                                      ),
                                    ),
                                  ),

                                ]),
                              ),
                            ) :

                            '${allUserName[index]}'.toLowerCase()
                                .contains(filter.toLowerCase())
                                ?
                            Card(
                              elevation: 2,
                              color: Colors.lightGreen[100],
                              child: SizedBox(
                                height: 360,
                                child: Column(children: [
                                  Container(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(
                                            10.0),
                                      ),
                                      color: Colors.white,
                                      child: Column(
                                        mainAxisSize:
                                        MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                              title: RichText(
                                                text: TextSpan(
                                                  style: defaultStyle,
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text:
                                                        'User Name :  ',
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .bold,
                                                            fontSize:
                                                            14)),
                                                    TextSpan(
                                                        text: allUserName[index]),
                                                  ],
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(
                                            10.0),
                                      ),
                                      color: Colors.white,
                                      child: Column(
                                        mainAxisSize:
                                        MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                              title: RichText(
                                                text: TextSpan(
                                                  style: defaultStyle,
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text:
                                                        'User PhoneNumber :  ',
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .bold,
                                                            fontSize:
                                                            14)),
                                                    TextSpan(
                                                        text: allUserPhoneNumber[index]),
                                                  ],
                                                ),
                                              )),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(
                                            10.0),
                                      ),
                                      color: Colors.white,
                                      child: Column(

                                        mainAxisSize:
                                        MainAxisSize.min,
                                        children: <Widget>[

                                          ListTile(
                                              title: RichText(
                                                text: TextSpan(
                                                  style: defaultStyle,
                                                  children: <TextSpan>[

                                                    TextSpan(
                                                        text:
                                                        'User Address:  ',
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .bold,
                                                            fontSize:
                                                            14)),
                                                    TextSpan(
                                                        text: allUserAddress[index]),
                                                  ],
                                                ),
                                              )),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(
                                            10.0),
                                      ),
                                      color: Colors.white,
                                      child: Column(
                                        mainAxisSize:
                                        MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                              title: RichText(
                                                text: TextSpan(
                                                  style: defaultStyle,
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text:
                                                        'Rating :    ',
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .bold,
                                                            fontSize:
                                                            14)),
                                                    TextSpan(
                                                        text: totalRating[index]),
                                                  ],
                                                ),
                                              )),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(
                                            10.0),
                                      ),
                                      color: Colors.white,
                                      child: Column(
                                        children: <Widget>[
                                          ListTile(
                                              title: RichText(
                                                text: TextSpan(
                                                  style: defaultStyle,
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text:
                                                        'Total Rating Count :    ',
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .bold,
                                                            fontSize:
                                                            14)),
                                                    TextSpan(
                                                        text: allUserCount[index].toString()
                                                           ),
                                                  ],
                                                ),
                                              )),
                                        ],
                                      ),
                                    ),
                                  ),
                                ]),
                              ),
                            ) : new Container();
                          }
                      ),
                    ),

                    SizedBox(height: 10.0),

                  ]
                      : <Widget>[
                    Container(
                        padding: EdgeInsets.fromLTRB(
                            50.0, 100.0, 50.0, 50.0),
                        child: Center(
                          child: Text(
                            "No data available",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 25.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Righteous',
                              //decoration: TextDecoration.none
                            ),
                          ),
                        ))
                  ]),
            ))
    );
  }

}
