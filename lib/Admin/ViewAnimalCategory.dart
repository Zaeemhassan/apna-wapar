import 'dart:io';

import 'package:apna_beepar/SellPets/SellAnimalsCategory.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:apna_beepar/Admin/AdminUpdateAnimalCategory.dart';
import 'package:apna_beepar/Admin/AdminHomePage.dart';

class ViewCategory extends StatefulWidget {
  @override
  _ViewCategoryState createState() => new _ViewCategoryState();
}

class _ViewCategoryState extends State<ViewCategory> {
  bool isLoading = true;
  var id;
  var categoryProduct;
  var documnet = [];
  List itemsData = [];
  var animalName = [];
  var animalImage = [];
  List document = [];
  var deleteCategoryData;
  bool dataCheck = false;

  Future getMenu() async {

    Firestore.instance
        .collection("Animal Category")
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        var menu = (result.data["Animal Category"]);

//        print("data $category");
        animalName.add(menu['AnimalName']);
        animalImage.add(menu['AnimalImage']);
        if (menu.isNotEmpty) {
          setState(() {
            dataCheck = true;
          });
        }
      });

      setState(() {
        isLoading = false;
      });
    });
  }

  Future deleteCategory(deleteCategoryData) async {
    Firestore.instance
        .collection("Animal Category")
        .document(deleteCategoryData)
        .delete()
        .then((_) {});
  }
  Future<bool> _onWillPop() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding:
            EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0, bottom: 0.0),
            children: <Widget>[
              Container(
                color: Colors.red,
                margin: EdgeInsets.all(0.0),
                padding: EdgeInsets.only(bottom: 10.0, top: 10.0),
                height: 100.0,
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.exit_to_app,
                        size: 30.0,
                        color: Colors.white,
                      ),
                      margin: EdgeInsets.only(bottom: 10.0),
                    ),
                    Text(
                      'Exit app',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Are you sure to exit app?',
                      style: TextStyle(color: Colors.white, fontSize: 14.0),
                    ),
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.cancel,
                        color: Colors.black,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'CANCEL',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: ()=> exit(0),

                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.check_circle,
                        color: Colors.black,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'YES',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          );
        }
    )??
        false;
  }
  @override
  void initState() {
    isLoading=true;
    getMenu();

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onWillPop,
        child:Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.lightGreen[500],
              title: Text(
                'Animal Category',
                style: TextStyle(
                    fontSize: 17, fontFamily: 'Righteous', color: Colors.white),
              ),
              bottom: PreferredSize(
                  child: Container(
                    color: Colors.black87,
                    height: 8.0,
                  ),
                  preferredSize: Size.fromHeight(8.0)),
            ),
            backgroundColor: Colors.white,
            body: isLoading
                ? SpinKitDoubleBounce(
              color: Colors.greenAccent[700],
            )
                : Center(
              child: ListView(
                  children:dataCheck
                      ?<Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: GridView.builder(
                          shrinkWrap: true,
                          physics: BouncingScrollPhysics(),
                          itemCount: animalName?.length ?? 0,
                          gridDelegate:
                          SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2),
                          itemBuilder: (context, index) => new InkWell(
                            child: Card(
                              elevation: 8,
                              semanticContainer: true,
                              color: Colors.white,
                              child: Container(
//                            width: 100.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.elliptical(60.0, 60.0)),
//                                  color: Colors.indigo[200],
                                ),
                                child: Column(

                                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                              child: IconButton(
                                                icon: Icon(
                                                  CupertinoIcons.pencil,
                                                ),
                                                onPressed: () {
                                                  Navigator.push(
                                                    context,
                                                    new MaterialPageRoute(
                                                        builder: (context) =>
                                                            UpdateCategory(
                                                                documentId:
                                                                document[
                                                                index],
                                                                updateAnimalName:
                                                                animalName[
                                                                index],
                                                                updateAnimalImage:
                                                                animalImage[
                                                                index])),
                                                  );
                                                },
                                              )),
                                          Expanded(
                                              child: IconButton(
                                                  icon: Icon(CupertinoIcons
                                                      .delete_solid),
                                                  onPressed: () {
//
                                                    deleteCategoryData =
                                                    document[index];
                                                   deleteCategory(deleteCategoryData);
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder:
                                                              (context) =>
                                                              ViewCategory()),
                                                    );
                                                  })),
                                        ],
                                      ),
                                      ClipRRect(
                                        borderRadius:
                                        BorderRadius.circular(15.0),
                                        child: Image.network(
                                          animalImage[index].toString(),
                                          height: 80.0,
                                          width: 140.0,
                                          fit: BoxFit.fill,
                                        ),
                                      ),

                                      Text(
                                        animalName[index].toString(),
                                        style: TextStyle(
                                            fontFamily: "Righteous",
                                            fontSize: 20),
                                      ),
                                    ]),
                              ),
                            ),
                          )),
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ]
                      : <Widget>[
                    Container(
                        padding: EdgeInsets.fromLTRB(
                            50.0, 100.0, 50.0, 50.0),
                        child: Center(
                          child: Text(
                            "First Create Animal Category",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Righteous',
                              //decoration: TextDecoration.none
                            ),
                          ),
                        ))
                  ]
              ),
            ))
    );
  }
}
