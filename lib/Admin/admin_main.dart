
import 'package:apna_beepar/Admin/AddCategory.dart';
import 'package:apna_beepar/Admin/AdminHomePage.dart';
import 'package:flutter/material.dart';
import 'package:apna_beepar/SellPets/SellAnimalsCategory.dart';
import 'package:localize_and_translate/localize_and_translate.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await translator.init(
    localeDefault: LocalizationDefaultType.device,
    languagesList: <String>['en', 'ur'],
    assetsDirectory: 'assets/translations/',
    apiKeyGoogle: '<Key>', // NOT YET TESTED
  );
  runApp(LocalizedApp(child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: translator.delegates,
      locale: translator.locale,
      supportedLocales: translator.locals(),
      home: AdminHomePage(),

    );
  }
}
