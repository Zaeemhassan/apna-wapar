
import 'package:apna_beepar/BuyPets/ViewBuyAnimalaList.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:share/share.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:flick_video_player/flick_video_player.dart';
import 'package:video_player/video_player.dart';

class ViewBuyAnimalDetail extends StatefulWidget {
  var animalPriceDetail;
  final String animalNameDetail;
  final String animalPregnancyStatus;
  final String animalPregnancyCount;
  var animalAllImages = [];
  final String animalVideoDetail;
  final String animalDescriptionDetail;
  final String animalLocationDetail;
  final String animalGenderDetail;
  final String animalBreedDetail;
  final String animalBuyerNameDetail;
  var animalPhoneNumberDetail;
  var animalSellerUserId;

  @override
  _ViewBuyAnimalDetailState createState() => new _ViewBuyAnimalDetailState();
  ViewBuyAnimalDetail(
      {Key key,
      @required this.animalDescriptionDetail,
        @required this.animalVideoDetail,
      @required this.animalPregnancyCount,
      @required this.animalPregnancyStatus,
      @required this.animalPriceDetail,
      @required this.animalNameDetail,
      @required this.animalLocationDetail,
      @required this.animalGenderDetail,
      @required this.animalBreedDetail,
      @required this.animalAllImages,
      @required this.animalBuyerNameDetail,
      @required this.animalPhoneNumberDetail,
      @required this.animalSellerUserId})
      : super(key: key);
}

class _ViewBuyAnimalDetailState extends State<ViewBuyAnimalDetail> {
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  var userId;
  var userProfileName;
  var userProfilePhone;
  var userProfileAddress;
  var document;
  double userProfileRating;
  var userProfileCount;

  Future getUserProfile() async {
    var firebaseUser = await FirebaseAuth.instance.currentUser();
    userId = firebaseUser.uid;
    Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .get()
        .then((value) {
      print(value.data);
      var id = value.documentID;
      document = (id);
      var user = value.data;

      userProfileName = (user['name']);
      userProfileAddress = (user['Address']);
      userProfilePhone = (user["PhoneNumber"]);
      userProfileRating = (user['rating']);
      userProfileCount = (user['count']);
      print("rating $userProfileRating");
    });
  }

  var id;
  double sellerRating;
  var sellerCount;
  String totalRating;
  double totalRatingWithOnlyOneDecimal;
  Future getSellerProfile() async {
    Firestore.instance
        .collection("users")
        .where('UserId', isEqualTo: widget.animalSellerUserId)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        var sellerData = result.data;
        sellerCount = sellerData['count'];
        sellerRating = sellerData['rating'];
      });
      totalRatingWithOnlyOneDecimal = sellerRating / sellerCount;
      totalRating = totalRatingWithOnlyOneDecimal.toStringAsFixed(1);
    });
  }

  int _current = 0;
  List images = [];
  String _currentAddress;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress = "${place.locality}, ${place.country}";
      });
      print('address $_currentAddress');
    } catch (e) {
      print(e);
    }
  }

  double userRating;
  Future updateUserProfile(userProfileRating, userProfileCount) async {
    Firestore.instance.collection("users").document(document).updateData({
      "name": userProfileName,
      "Address": userProfileAddress,
      "PhoneNumber": userProfilePhone,
      "rating": userProfileRating,
      "count": userProfileCount
    }).then((value) {});
//
  }
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo();
  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
        adUnitId: "ca-app-pub-9603407988877715/8189731030",
        //Change Interstitial AdUnitId with Admob ID
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("IntersttialAd $event");
        });
  }
FlickManager flickManager;
  @override
  void initState() {
    super.initState();
    print("Animal ${widget.animalVideoDetail}");
    _getCurrentLocation();
    getSellerProfile();
    getUserProfile();
    flickManager =FlickManager(
        videoPlayerController: VideoPlayerController.network(widget.animalVideoDetail));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.lightGreen[500],
              title: _getLocation(),
              bottom: PreferredSize(
                  child: Container(
                    color: Colors.black87,
                    height: 8.0,
                  ),
                  preferredSize: Size.fromHeight(8.0)),

            ),
            backgroundColor: Colors.white,
            body: Center(
              child: ListView(children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                Column(children: [
                  CarouselSlider(
                      options: CarouselOptions(
                          aspectRatio: 20 / 14,
                          scrollDirection: Axis.horizontal,
                          enlargeCenterPage: true,
                          reverse: false,
                          viewportFraction: 1.0,
                          onPageChanged: (index, reason) {
                            setState(() {
                              _current = index;
                            });
                          }),
                      items: map<Widget>(
                        widget.animalAllImages,
                        (index, i) {
                          return Container(
                            margin: EdgeInsets.all(7.0),
                            child: ClipRRect(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                              child: Stack(children: <Widget>[
                                FadeInImage(
                                  image: NetworkImage(i.toString()),
                                  fit: BoxFit.cover,
                                  width: 1000,
                                  placeholder:
                                      AssetImage('assets/images/loading.gif'),
                                ),
                                InkWell(
                                  child: FadeInImage(
                                    image: NetworkImage(i.toString()),
                                    fit: BoxFit.cover,
                                    width: 1000,
                                    placeholder:
                                        AssetImage('assets/images/loading.gif'),
                                  ),
                                ),
                              ]),
                            ),
                          );
                        },
                      ).toList()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: map<Widget>(
                      widget.animalAllImages,
                      (index, url) {
                        return Container(
                          width: 8.0,
                          height: 8.0,
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 2.0),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: _current == index
                                  ? Color.fromRGBO(0, 0, 0, 0.9)
                                  : Color.fromRGBO(0, 0, 0, 0.4)),
                        );
                      },
                    ),
                  ),
                ]),
                Container(
                  margin: const EdgeInsets.all(10),
                  child:Text(
                    "Animal Video",
                    style: TextStyle(fontFamily: 'Righteous',fontSize: 17),
                  )
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child:ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: (widget.animalVideoDetail != null)
                          ? AspectRatio(
                          aspectRatio: 20 / 10,
                          child: FlickVideoPlayer(
                            flickManager: flickManager,
                            flickVideoWithControls: FlickVideoWithControls(
                              controls: FlickPortraitControls(),
                            ),
                          )
                      )
                          : Center(
                        child:Text(
                          "No Video Available",
                          style: TextStyle(fontFamily: 'Righteous',fontSize: 16),
                        )
                      )

                    ),
                  ),

                SizedBox(
                  height: 20,
                ),
                Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 5, 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                            child: Text(
                              translator.translate("Seller Name: ") + widget.animalBuyerNameDetail,
                          style:
                              TextStyle(fontFamily: 'Righteous', fontSize: 16),
                        )),
                        Expanded(
                          flex: 0,
                          child: IconButton(
                            icon: Image.asset('assets/images/message.png'),
                            onPressed: ()=> launch(
                             "sms:${widget.animalPhoneNumberDetail}"),

                          ),
                        ),
                        Expanded(
                          flex: 0,
                          child: IconButton(
                            icon: Image.asset('assets/images/apple.png'),
                            onPressed: () {
                              createInterstitialAd()
                                ..load()
                                ..show();
                                launch("tel:${widget.animalPhoneNumberDetail}");},
                          ),
                        )
                      ],
                    )),
                Container(
                  margin: const EdgeInsets.all(6),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Colors.lightGreen[100],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                            title: Row(
                          children: [
                            Expanded(
                              child: Text(translator.translate("Rating : "),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Righteous',
                                      fontSize: 16)),
                            ),
                            Expanded(flex: -3, child:_profileRating()),
                            Expanded(
                                flex: -2,
                                child: Icon(
                                  Icons.star,
                                  color: Colors.yellow[700],
                                )),
                            Expanded(
                                flex: 2,
                                child:_profileCount()
                            )
                          ],
                        )),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(5),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Colors.lightGreen[100],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          title: Text(
                        translator.translate("Location: ") +
                                  widget.animalLocationDetail.toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Righteous',
                                  fontSize: 16)),
                          leading: Icon(Icons.location_on_sharp),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(5),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Colors.lightGreen[100],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          title: Text(
                        translator.translate("Animal Category: ") +
                                  widget.animalNameDetail.toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Righteous',
                                  fontSize: 15)),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(5),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Colors.lightGreen[100],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          title: Text(
                        translator.translate("Price: ") +
                                  widget.animalPriceDetail.toString() +
                            translator.translate("Rs."),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Righteous',
                                  fontSize: 15)),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(5),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Colors.lightGreen[100],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          title: Text(
                        translator.translate("Breed: ") + widget.animalBreedDetail.toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Righteous',
                                  fontSize: 15)),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(5),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Colors.lightGreen[100],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          title: Text(
                        translator.translate("Pregnancy Status: ") +
                        translator.translate(widget.animalPregnancyStatus.toString()) ??
                                  Text("No data"),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Righteous',
                                  fontSize: 15)),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(5),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Colors.lightGreen[100],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          title: Text(
                        translator.translate("Pregnancy Count: ") +
                        translator.translate(widget.animalPregnancyCount.toString()) ??
                                  Text("No data"),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Righteous',
                                  fontSize: 15)),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 200,
                  margin: const EdgeInsets.all(5),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: Colors.lightGreen[100],
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ListTile(
                          title: Text(
                        translator.translate("Description: ") +
                                  widget.animalDescriptionDetail.toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Righteous',
                                  fontSize: 15)),
                          leading: Icon(Icons.note),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                    height: 50.0,
                    width: 400,
                    padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 0.0),
                    child: RaisedButton.icon(
                      icon: Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                      shape: RoundedRectangleBorder(side: BorderSide()),
                      color: Colors.lightGreen[600],
                      onPressed: () {
                        createInterstitialAd()
                          ..load()
                          ..show();
                        showRating();
                      },
                      label: Text(
                        translator.translate('Give a rating'),
                        style: TextStyle(
                            fontFamily: 'Righteous',
                            color: Colors.white,
                            fontSize: 17),
                      ),
                    )),
                SizedBox(
                  height: 20,
                ),
                Container(
                    height: 50.0,
                    width: 400,
                    padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 0.0),
                    child: RaisedButton.icon(
                      icon: Icon(
                        CupertinoIcons.share,
                        color: Colors.white,
                      ),
                      shape: RoundedRectangleBorder(side: BorderSide()),
                      color: Colors.lightGreen[600],
                      onPressed: () {
                        createInterstitialAd()
                          ..load()
                          ..show();
                        final RenderBox box = context.findRenderObject();
                        Share.share(
                            "Apna Wapar"
                            " "
                            'https://apnawaparsharepost.page.link/sharepost'
                            "                                            "
                            "Visit our app to see post",
                            sharePositionOrigin:
                                box.localToGlobal(Offset.zero) & box.size);
                      },
                      label: Text(
                        translator.translate('Share Post'),
                        style: TextStyle(
                            fontFamily: 'Righteous',
                            color: Colors.white,
                            fontSize: 17),
                      ),
                    )),
                SizedBox(
                  height: 20,
                )
              ]),
            ));
  }

  void showRating() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              width: 260.0,
              height: 110.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(60.0)),
              ),
              child: Column(children: [
                Center(
                    child: Text(
                      translator.translate('Give a rating'),
                  style: TextStyle(fontSize: 20, fontFamily: 'Righteous'),
                )),
                SizedBox(
                  height: 20,
                ),
                RatingBar.builder(
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 3.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    userRating = rating;
                    print(userRating);
                  },
                ),
              ]),
            ),
            backgroundColor: Colors.white,
            actions: [
              FlatButton(
                child: Text(translator.translate('Ok')),
                onPressed: () {
                  userProfileRating = userProfileRating + userRating;
                  print("rating $userProfileRating");
                  userProfileCount = userProfileCount + 1;
                  updateUserProfile(userProfileRating, userProfileCount);
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text(translator.translate('Cancel')),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }
  Widget _profileRating() {
    if(userProfileRating == null) {
      return Text("Loading...", style: TextStyle(fontFamily: 'Righteous', fontSize: 16),);
    }
     else if (userProfileRating == 0.0) {
        return Text(
          userProfileRating.toString(),
          style: TextStyle(fontFamily: 'Righteous', fontSize: 16),
        );
      }

    else {
      return Text(
        totalRating.toString(),
        style: TextStyle(fontFamily: 'Righteous', fontSize: 16),
      );
    }
  }
  Widget _profileCount() {
    if (sellerCount == null) {
      return Text(
       "(" +"0"+ ")",
        style: TextStyle(fontFamily: 'Righteous', fontSize: 16),
      );
    }
    else {
      return Text(
       "(" + "$sellerCount" + ")",
        style: TextStyle(fontFamily: 'Righteous', fontSize: 16),
      );
    }
  }
  Widget _getLocation() {
    if (_currentAddress == null) {
      return Text(
        "Loading...",
        style: TextStyle(fontFamily: 'Righteous', fontSize: 17),
      );
    }
    else {
      return Text(
        "$_currentAddress",
        style: TextStyle(fontFamily: 'Righteous', fontSize: 17),
      );
    }
  }

}
