import 'package:apna_beepar/BuyPets/ViewBuyAnimalListDetails.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:video_player/video_player.dart';
import 'package:apna_beepar/UserHomePage/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geolocator/geolocator.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:video_player/video_player.dart';

import '../timeago.dart';

class ViewBuyAnimalList extends StatefulWidget {
  var animalListName;

  @override
  _ViewBuyAnimalListState createState() => new _ViewBuyAnimalListState();
  ViewBuyAnimalList({
    Key key,
    @required this.animalListName,
  }) : super(key: key);
}

class _ViewBuyAnimalListState extends State<ViewBuyAnimalList> {
  bool isLoading = false;
  var id;
  String radioButtonItem = translator.translate('All');
  String radioButton = translator.translate('Date');
  int typeId = 1;
  int allId = 1;
  String radioCategory = translator.translate('Not Pregnant');
  int catId = 1;
  List itemsData = [];
  List animalName = [];
  var animalImage1 = [];
  var animalImage2 = [];
  var animalImage3 = [];
  var animalPrice = [];
  var animalSellerLocation = [];
  var animalCategory = [];
  var pregnancyCount = [];
  var animalGender = [];
  var pregnancyStatus = [];
  var animalBreed = [];
  var animalDescription = [];
  var animalBuyerName = [];
  var animalBuyerPhone = [];
  var animalUserId = [];
  List city = [];
  var selectedCity, selectedTypeCity;
  var selectedStatus, selectedTypeStatus;
  var pregnanciesStatus = [
    translator.translate('Not Pregnant'),
    translator.translate('First Month'),
    translator.translate('Second Month'),
    translator.translate('Third Month'),
    translator.translate('Fourth Month'),
    translator.translate('Fifth Month'),
    translator.translate('Sixth Month'),
    translator.translate('Seventh Month'),
    translator.translate('Eighth Month'),
    translator.translate('Ninth Month'),
    translator.translate('Tenth Month')
  ];
  var animalVideo = [];
  List document = [];
  var allImages = [];
  var sellingData;
  var allAnimalsData = [];
  var dateTime = [];
  var deleteCategoryData;
  VideoPlayerController _controller;
  bool dataCheck = false;
  String _currentAddress;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  TextEditingController searchController = new TextEditingController();
  String filter;
  RangeValues values = RangeValues(1, 1000000);
  RangeLabels labels =RangeLabels('1', "1000000");
  Future getAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .where("SellAnimalList.AnimalCategoryName",isEqualTo: widget.animalListName)
        .orderBy("SellAnimalList.DateTime", descending: true)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        animalName.add(sellingData['AnimalCategoryName']);


            allAnimalsData.add(result.data['SellAnimalList']);
            animalUserId.add(sellingData['UserId']);
            animalBuyerName.add(sellingData['UserName']);
            animalBuyerPhone.add(sellingData['UserPhoneNumber']);
            animalImage1.add(sellingData['AnimalImage1']);
            animalImage2.add(sellingData['AnimalImage2']);
            animalImage3.add(sellingData['AnimalImage3']);
            animalVideo.add(sellingData["AnimalVideo"]);
            animalBreed.add(sellingData['AnimalBreed']);
            animalSellerLocation.add(sellingData['AnimalLocation']);
            pregnancyCount.add(sellingData['AnimalPregnancyCount']);
            pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
            animalGender.add(sellingData['AnimalGender']);
            animalDescription.add(sellingData['AnimalDescription']);
            animalPrice.add(sellingData['AnimalPrice']);
            city.add(sellingData["AnimalCity"]);
            dateTime.add(sellingData["DateTime"]);
      });
      print("city $city");
      setState(() {
        isLoading = false;
      });
    });
  }

  Future getTodayAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .where('SellAnimalList.DateTime',
            isGreaterThan: DateTime.now().subtract(Duration(days: 1)))
        .where('SellAnimalList.AnimalCategoryName',
        isEqualTo: widget.animalListName)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
        city.add(sellingData["AnimalCity"]);
        dateTime.add(sellingData["DateTime"]);
      });
      print("Animal video $animalVideo");
      setState(() {
        isLoading = false;
      });
    });
  }
  Future getGenderAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .where('SellAnimalList.AnimalGender',
        isEqualTo: radioButton)
        .where('SellAnimalList.AnimalCategoryName',
        isEqualTo: widget.animalListName)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        city.add(sellingData["AnimalCity"]);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
        dateTime.add(sellingData["DateTime"]);
      });
      print("Animal video $animalVideo");
      setState(() {
        isLoading = false;
      });
    });
  }
  Future getCurrentLocationAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .where('SellAnimalList.AnimalLocation',
        isEqualTo: _currentAddress)
        .where('SellAnimalList.AnimalCategoryName',
        isEqualTo: widget.animalListName)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        city.add(sellingData["AnimalCity"]);
        animalPrice.add(sellingData['AnimalPrice']);
        dateTime.add(sellingData["DateTime"]);
      });
      print("Animal video $animalVideo");
      setState(() {
        isLoading = false;
      });
    });
  }
  Future getPregnancyAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .where('SellAnimalList.AnimalPregnancyStatus',
        isEqualTo: selectedTypeStatus)
        .where('SellAnimalList.AnimalCategoryName',
        isEqualTo: widget.animalListName)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
        city.add(sellingData["AnimalCity"]);
        dateTime.add(sellingData["DateTime"]);
      });
      print("Animal video $animalVideo");
      setState(() {
        isLoading = false;
      });
    });
  }

  Future get48HourAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .where('SellAnimalList.DateTime',
            isGreaterThan: DateTime.now().subtract(Duration(days: 2)))
        .where('SellAnimalList.AnimalCategoryName',
        isEqualTo: widget.animalListName)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
        city.add(sellingData["AnimalCity"]);
        dateTime.add(sellingData["DateTime"]);
      });
      setState(() {
        isLoading = false;
      });
    });
  }

  Future getPriceLowToHighAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .where("SellAnimalList.AnimalCategoryName",isEqualTo: widget.animalListName)
        .orderBy("SellAnimalList.AnimalPrice", descending: false)

        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        animalName.add(sellingData['AnimalCategoryName']);

            allAnimalsData.add(result.data['SellAnimalList']);
            animalUserId.add(sellingData['UserId']);
            animalBuyerName.add(sellingData['UserName']);
            animalBuyerPhone.add(sellingData['UserPhoneNumber']);
            animalImage1.add(sellingData['AnimalImage1']);
            animalImage2.add(sellingData['AnimalImage2']);
            animalImage3.add(sellingData['AnimalImage3']);
            animalVideo.add(sellingData["AnimalVideo"]);
            animalBreed.add(sellingData['AnimalBreed']);
            animalSellerLocation.add(sellingData['AnimalLocation']);
            pregnancyCount.add(sellingData['AnimalPregnancyCount']);
            pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
            animalGender.add(sellingData['AnimalGender']);
            animalDescription.add(sellingData['AnimalDescription']);
            animalPrice.add(sellingData['AnimalPrice']);
        city.add(sellingData["AnimalCity"]);
        dateTime.add(sellingData["DateTime"]);

      });
      setState(() {
        isLoading = false;
      });
    });
  }

  Future getDateWiseAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .where('SellAnimalList.AnimalCategoryName',
        isEqualTo: widget.animalListName)
        .orderBy("SellAnimalList.DateTime", descending: true)

        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId.add(sellingData['UserId']);
        animalName.add(sellingData['AnimalCategoryName']);
        animalBuyerName.add(sellingData['UserName']);
        animalBuyerPhone.add(sellingData['UserPhoneNumber']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalVideo.add(sellingData["AnimalVideo"]);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
        city.add(sellingData["AnimalCity"]);
        dateTime.add(sellingData["DateTime"]);
      });
      setState(() {
        isLoading = false;
      });
    });
  }

  Future getPriceHighToLowAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });

    Firestore.instance
        .collection("SellAnimalList")
        .where('SellAnimalList.AnimalCategoryName',
        isEqualTo: widget.animalListName)
        .orderBy("SellAnimalList.AnimalPrice", descending: true)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);

        animalName.add(sellingData['AnimalCategoryName']);
        for (int i = 0; i < animalName.length; i++) {
          if(widget.animalListName == animalName[i]){
          sellingData = (result.data["SellAnimalList"]);
          allAnimalsData.add(result.data['SellAnimalList']);
          animalUserId.add(sellingData['UserId']);
          animalBuyerName.add(sellingData['UserName']);
          animalBuyerPhone.add(sellingData['UserPhoneNumber']);
          animalImage1.add(sellingData['AnimalImage1']);
          animalImage2.add(sellingData['AnimalImage2']);
          animalImage3.add(sellingData['AnimalImage3']);
          animalVideo.add(sellingData["AnimalVideo"]);
          animalBreed.add(sellingData['AnimalBreed']);
          animalSellerLocation.add(sellingData['AnimalLocation']);
          pregnancyCount.add(sellingData['AnimalPregnancyCount']);
          pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
          animalGender.add(sellingData['AnimalGender']);
          city.add(sellingData["AnimalCity"]);
          animalDescription.add(sellingData['AnimalDescription']);
          animalPrice.add(sellingData['AnimalPrice']);
          dateTime.add(sellingData["DateTime"]);
        }
      }
      });
      setState(() {
        isLoading = false;
      });
    });
  }

  _getCurrentLocation() {
    setState(() {
      isLoading = true;
    });
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
      setState(() {
        isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress = "${place.locality}, ${place.country}";
      });
      print('address $_currentAddress');
    } catch (e) {
      print(e);
    }
  }

  var userId;
  var name;
  Future getUserProfile() async {
    setState(() {
      isLoading = true;
    });
    var firebaseUser = await FirebaseAuth.instance.currentUser();
    userId = firebaseUser.uid;
    Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .get()
        .then((value) {
      print(value.data);
      var id = value.documentID;
      var user = value.data;
      name = user["name"];
      setState(() {
        isLoading = false;
      });
    });
  }
  BannerAd _bannerAd;
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo();
  BannerAd createBannerAd() {
    return new BannerAd(
        adUnitId: "ca-app-pub-9603407988877715/3498665226",
        size: AdSize.banner,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("Banner event : $event");
        });
  }
  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
        adUnitId: "ca-app-pub-9603407988877715/8189731030",
        //Change Interstitial AdUnitId with Admob ID
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("IntersttialAd $event");
        });
  }
  final FirebaseAuth auth = FirebaseAuth.instance;
  var currentDateTime;
  @override
  void initState() {
    FirebaseAdMob.instance
        .initialize(appId: "ca-app-pub-9603407988877715~1895785423");
    _bannerAd = createBannerAd()
      ..load()
      ..show();
    _getCurrentLocation();
    super.initState();
    getAnimalSellingData();
    getUserProfile();
    currentDateTime = DateTime.now();
    searchController.addListener(() {
      setState(() {
        filter = searchController.text;
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bannerAd?.dispose();
    searchController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightGreen[500],
          title: _getLocation(),
          bottom: PreferredSize(
              child: Container(
                color: Colors.black87,
                height: 8.0,
              ),
              preferredSize: Size.fromHeight(8.0)),

        ),
        backgroundColor: Colors.white,
        body: isLoading
            ? SpinKitDoubleBounce(
                color: Colors.greenAccent[700],
              )
            : Column(children: <Widget>[

              buildCity(),
          buildStatus(),
                Container(
                  child: InkWell(
                    onTap: () async {
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => ViewPersonalSellingList()));
                    },
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      color: Colors.white,
                      elevation: 1,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                              title: Row(
                            children: [
                              Expanded(
                                child: SizedBox(
                                    height: 60,
                                    width: 70,
                                    child: InkWell(
                                        onTap: () {
                                          showAll();
                                        },
                                        child: Card(
                                            child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Icon(Icons.arrow_drop_down_sharp),
                                            Text(
                                              '$radioButtonItem',
                                              style: TextStyle(
                                                  fontFamily: 'Righteous',
                                                  fontSize: 14),
                                            ),
                                          ],
                                        )))),
                              ),
                              Expanded(
                                  child: SizedBox(
                                      height: 60,
                                      width: 70,
                                      child: InkWell(
                                          onTap: () {
                                            showSort();
                                          },
                                          child: Card(
                                              child: Center(
                                            child: Text(
                                              translator.translate('Sort'),
                                              style: TextStyle(
                                                  fontFamily: 'Righteous',
                                                  fontSize: 14),
                                            ),
                                          ))))),
                            ],
                          )),
                        ],
                      ),
                    ),
                  ),
                ),

                SizedBox(
                  height: 10,
                ),

                Expanded(
                  child: ListView(children: [
                    Container(
                        margin: const EdgeInsets.all(10),
                        child: GridView.builder(
                            shrinkWrap: true,
                            physics: BouncingScrollPhysics(),
                            itemCount: animalName?.length ?? 0,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 1),
                            itemBuilder: (context, index) {
                              return InkWell(
                                      splashColor: Colors.green,
                                      onTap: () {
                                        allImages.clear();
                                        allImages.add(animalImage1[index]);
                                        allImages.add(animalImage2[index]);
                                        allImages.add(animalImage3[index]);
                                        Navigator.push(
                                          context,
                                          new MaterialPageRoute(
                                              builder: (context) =>
                                                  ViewBuyAnimalDetail(
                                                    animalNameDetail:
                                                        animalName[index],
                                                    animalLocationDetail:
                                                        animalSellerLocation[
                                                            index],
                                                    animalDescriptionDetail:
                                                        animalDescription[
                                                            index],
                                                    animalGenderDetail:
                                                        animalGender[index],
                                                    animalAllImages: allImages,
                                                    animalPregnancyCount:
                                                        pregnancyCount[index],
                                                    animalPregnancyStatus:
                                                        pregnancyStatus[index],
                                                    animalPriceDetail:
                                                        animalPrice[index],
                                                    animalBreedDetail:
                                                        animalBreed[index],
                                                    animalBuyerNameDetail:
                                                        animalBuyerName[index],
                                                    animalPhoneNumberDetail:
                                                        animalBuyerPhone[index],
                                                    animalSellerUserId:
                                                        animalUserId[index],
                                                    animalVideoDetail:
                                                        animalVideo[index],
                                                  )),
                                        );
                                      },
                                      child: Card(
                                        elevation: 8,
                                        semanticContainer: true,
                                        color: Colors.white,
                                        child: Column(children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            child: Image.network(
                                              animalImage1[index].toString(),
                                              height: 200.0,
                                              width: 400.0,
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                          Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  10, 5, 10, 0),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                      translator.translate(
                                                          "Animal Category: "),
                                                      style: TextStyle(
                                                          fontFamily:
                                                              "Righteous",
                                                          fontSize: 17),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(
                                                      animalName[index]
                                                          .toString(),
                                                      style: TextStyle(
                                                          fontFamily:
                                                              "Righteous",
                                                          fontSize: 17),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child:  Text(
                                                      TimeAgo.timeAgoSinceDate(dateTime[index]),
                                                      style: TextStyle(fontSize: 14, color: Colors.black54),
                                                    ),
                                                  )
                                                ],
                                              )),
                                          Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  10, 5, 10, 0),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                      translator
                                                          .translate("Price: "),
                                                      style: TextStyle(
                                                          fontFamily:
                                                              "Righteous",
                                                          fontSize: 17),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 3,
                                                    child: Text(
                                                      animalPrice[index]
                                                              .toString() +
                                                          translator
                                                              .translate("Rs."),
                                                      style: TextStyle(
                                                          fontFamily:
                                                              "Righteous",
                                                          fontSize: 17),
                                                    ),
                                                  )
                                                ],
                                              )),
                                          Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  10, 0, 5, 10),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Spacer(),
                                                  Spacer(),
                                                  Spacer(),
                                                  Spacer(),
                                                  Spacer(),
                                                  Spacer(),
                                                  Expanded(
                                                    flex: 2,
                                                    child: IconButton(
                                                      icon: Image.asset(
                                                          'assets/images/message.png'),
                                                      onPressed: () => launch(
                                                          "sms:${animalBuyerPhone[index]}"),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 0,
                                                    child: IconButton(
                                                      icon: Image.asset(
                                                          'assets/images/apple.png'),
                                                      onPressed: () {
                                                        createInterstitialAd()
                                                          ..load()
                                                          ..show();
                                                        launch
                                                      (
                                                          "tel:${animalBuyerPhone[index]}");}
                                                    ),
                                                  ),
                                                ],
                                              )),
                                        ]),
                                      ),
                                    );

                            })),
                    SizedBox(
                      height: 20,
                    )
                  ]),
                )
              ]));
  }

  void showAll() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              width: 260.0,
              height: 200.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(60.0)),
              ),
              child: Column(children: [
                Row(children: [
                  Radio(
                    value: 1,
                    groupValue: id,
                    onChanged: (val) {
                      setState(() {
                        radioButtonItem = 'All';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getAnimalSellingData();
                        Navigator.of(context).pop();

                        id = 1;
                      });
                    },
                  ),
                  Text(
                    translator.translate('All'),
                    style: new TextStyle(fontSize: 14.0),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 2,
                    groupValue: id,
                    onChanged: (val) {
                      setState(() {
                        radioButtonItem = 'Current Location';
                        getCurrentLocationAnimalSellingData();
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        Navigator.of(context).pop();
                        id = 2;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Current Location'),
                    style: new TextStyle(
                      fontSize: 14.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 3,
                    groupValue: id,
                    onChanged: (val) {
                      setState(() {
                        radioButtonItem = 'Today';
                        getTodayAnimalSellingData();
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        Navigator.of(context).pop();
                        id = 3;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Today'),
                    style: new TextStyle(
                      fontSize: 14.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 4,
                    groupValue: id,
                    onChanged: (val) {
                      setState(() {
                        radioButtonItem = 'In 48 hours';
                        get48HourAnimalSellingData();
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        pregnancyStatus.clear();
                        animalVideo.clear();
                        pregnancyCount.clear();
                        city.clear();
                        Navigator.of(context).pop();
                        id = 4;
                      });
                    },
                  ),
                  Text(
                    translator.translate('In 48 hours'),
                    style: new TextStyle(
                      fontSize: 14.0,
                    ),
                  ),
                ]),
              ]),
            ),
            backgroundColor: Colors.white,
            actions: [
              FlatButton(
                child: Text(translator.translate('Cancel')),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });

//
  }

  void showSort() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              width: 260.0,
              height: 240.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(60.0)),
              ),
              child: Column(children: [
                Row(children: [
                  Radio(
                    value: 1,
                    groupValue: allId,
                    onChanged: (val) {
                      setState(() {
                        radioButton = 'Date';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getDateWiseAnimalSellingData();
                        Navigator.of(context).pop();
                        allId = 1;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Date'),
                    style: new TextStyle(fontSize: 14.0),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 2,
                    groupValue: allId,
                    onChanged: (val) {
                      setState(() {
                        radioButton = 'Price low to high';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getPriceLowToHighAnimalSellingData();
                        Navigator.of(context).pop();
                        allId = 2;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Price low to high'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 3,
                    groupValue: allId,
                    onChanged: (val) {
                      setState(() {
                        radioButton = 'Price high to low';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalVideo.clear();
                        animalBreed.clear();
                        pregnancyStatus.clear();
                        city.clear();
                        pregnancyCount.clear();
                        getPriceHighToLowAnimalSellingData();
                        Navigator.of(context).pop();
                        allId = 3;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Price high to low'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 4,
                    groupValue: allId,
                    onChanged: (val) {
                      setState(() {
                        radioButton = "Male";
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalVideo.clear();
                        animalBreed.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getGenderAnimalSellingData();
                        Navigator.of(context).pop();
                        allId = 4;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Male'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 5,
                    groupValue: allId,
                    onChanged: (val) {
                      setState(() {
                        radioButton = "Female";
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalVideo.clear();
                        animalBreed.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getGenderAnimalSellingData();
                        Navigator.of(context).pop();
                        allId = 5;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Female'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
              ]),
            ),
            backgroundColor: Colors.white,
            actions: [
              FlatButton(
                child: Text(translator.translate('Cancel')),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });

//
  }
  void showCategory() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              width: 260.0,
              height: 600.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(60.0)),
              ),
              child: Column(children: [
                Row(children: [
                  Radio(
                    value: 1,
                    groupValue: catId,
                    onChanged: (val) {
                      setState(() {
                        radioCategory = 'Not Pregnant';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getPregnancyAnimalSellingData();
                        Navigator.of(context).pop();
                        catId = 1;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Not Pregnant'),
                    style: new TextStyle(fontSize: 14.0),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 2,
                    groupValue: catId,
                    onChanged: (val) {
                      setState(() {
                        radioCategory = 'First Month';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                      getPregnancyAnimalSellingData();
                        Navigator.of(context).pop();
                        catId = 2;
                      });
                    },
                  ),
                  Text(
                    translator.translate('First Month'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 3,
                    groupValue: catId,
                    onChanged: (val) {
                      setState(() {
                        radioCategory = 'Second Month';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getPregnancyAnimalSellingData();
                        Navigator.of(context).pop();
                        catId = 3;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Second Month'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 4,
                    groupValue: catId,
                    onChanged: (val) {
                      setState(() {
                        radioCategory = 'Third Month';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getPregnancyAnimalSellingData();
                        Navigator.of(context).pop();
                        catId = 4;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Third Month'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 5,
                    groupValue: catId,
                    onChanged: (val) {
                      setState(() {
                        radioCategory = 'Fourth Month';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getPregnancyAnimalSellingData();
                        Navigator.of(context).pop();
                        catId = 5;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Fourth Month'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 6,
                    groupValue: catId,
                    onChanged: (val) {
                      setState(() {
                        radioCategory = 'Fifth Month';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getPregnancyAnimalSellingData();
                        Navigator.of(context).pop();
                        catId = 6;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Fifth Month'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 7,
                    groupValue: catId,
                    onChanged: (val) {
                      setState(() {
                        radioCategory = 'Sixth Month';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getPregnancyAnimalSellingData();
                        Navigator.of(context).pop();
                        catId = 7;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Sixth Month'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 8,
                    groupValue: catId,
                    onChanged: (val) {
                      setState(() {
                        radioCategory = 'Seventh Month';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getPregnancyAnimalSellingData();
                        Navigator.of(context).pop();
                        catId = 8;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Seventh Month'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 9,
                    groupValue: catId,
                    onChanged: (val) {
                      setState(() {
                        radioCategory = 'Eighth Month';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getPregnancyAnimalSellingData();
                        Navigator.of(context).pop();
                        catId = 9;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Eighth Month'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 10,
                    groupValue: catId,
                    onChanged: (val) {
                      setState(() {
                        radioCategory = 'Ninth Month';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        city.clear();
                        pregnancyCount.clear();
                        getPregnancyAnimalSellingData();
                        Navigator.of(context).pop();
                        catId = 10;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Ninth Month'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 11,
                    groupValue: catId,
                    onChanged: (val) {
                      setState(() {
                        radioCategory = 'Tenth Month';
                        animalName.clear();
                        animalBuyerName.clear();
                        animalBuyerPhone.clear();
                        animalSellerLocation.clear();
                        animalDescription.clear();
                        animalImage1.clear();
                        animalPrice.clear();
                        animalImage2.clear();
                        animalImage3.clear();
                        animalGender.clear();
                        animalBreed.clear();
                        animalVideo.clear();
                        pregnancyStatus.clear();
                        pregnancyCount.clear();
                        city.clear();
                        getPregnancyAnimalSellingData();
                        Navigator.of(context).pop();
                        catId = 11;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Tenth Month'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
              ]),
            ),
            backgroundColor: Colors.white,
            actions: [
              FlatButton(
                child: Text(translator.translate('Cancel')),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });

//
  }
  Widget _getLocation() {
    if (_currentAddress == null) {
      return Text(
       "Loading...",
        style: TextStyle(fontFamily: 'Righteous', fontSize: 17),
      );
    }
    else {
      return Text(
       "$_currentAddress",
        style: TextStyle(fontFamily: 'Righteous', fontSize: 17),
      );
    }
  }
  Widget buildCity() {
    return Container(
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3.0),
          border: Border.all(width: 2, color: Colors.black26),
        ),
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: DropdownButtonHideUnderline(
            child: DropdownButton(
              items: city
                  .map((value) => DropdownMenuItem(
                child: Text(
                  value,
                  style: TextStyle(color: Colors.black),
                ),
                value: value,
              ))
                  .toList(),
              onChanged: (selectedCity) {
                setState(() {
                  selectedTypeCity = selectedCity;
                    animalName.clear();
                    animalBuyerName.clear();
                    animalBuyerPhone.clear();
                    animalSellerLocation.clear();
                    animalDescription.clear();
                    animalImage1.clear();
                    animalPrice.clear();
                    animalImage2.clear();
                    animalImage3.clear();
                    animalGender.clear();
                    animalBreed.clear();
                    animalVideo.clear();
                    pregnancyStatus.clear();
                    pregnancyCount.clear();
                    city.clear();
                    getAnimalSellingData();

                });
              },
              value: selectedTypeCity,
              isExpanded: true,
              hint: Text(
                ('City'),
                style: TextStyle(color: Colors.black),
              ),
            )));
  }
  Widget buildStatus() {
    return Container(
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3.0),
          border: Border.all(width: 2, color: Colors.black26),
        ),
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: DropdownButtonHideUnderline(
            child: DropdownButton(
              items: pregnanciesStatus
                  .map((value) => DropdownMenuItem(
                child: Text(
                  value,
                  style: TextStyle(color: Colors.black),
                ),
                value: value,
              ))
                  .toList(),
              onChanged: (selectedStatus) {
                setState(() {
                  selectedTypeStatus = selectedStatus;
                  animalName.clear();
                  animalBuyerName.clear();
                  animalBuyerPhone.clear();
                  animalSellerLocation.clear();
                  animalDescription.clear();
                  animalImage1.clear();
                  animalPrice.clear();
                  animalImage2.clear();
                  animalImage3.clear();
                  animalGender.clear();
                  animalBreed.clear();
                  animalVideo.clear();
                  pregnancyStatus.clear();
                  pregnancyCount.clear();
                  city.clear();
                  getPregnancyAnimalSellingData();

                });
              },
              value: selectedTypeStatus,
              isExpanded: true,
              hint: Text(
                ('Pregnancy Status'),
                style: TextStyle(color: Colors.black),
              ),
            )));
  }
}
