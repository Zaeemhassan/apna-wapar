
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:apna_beepar/UserHomePage/HomePage.dart';


class FilterPage extends StatefulWidget {
  @override
  _FilterPageState createState() => new _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
//
  String radioButtonItem = translator.translate('All');
  String radioButton = translator.translate('Date');
  int typeId = 1;
  int allId = 1;
  String radioCategory = translator.translate('All');
  int catId = 1;

  @override
  void initState() {
    super.initState();

  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[500],
        title: Text(
          translator.translate('Filter'),
          style: TextStyle(
              fontSize: 18, fontFamily: 'Righteous', color: Colors.white),
        ),
        bottom: PreferredSize(
            child: Container(
              color: Colors.black87,
              height: 8.0,
            ),
            preferredSize: Size.fromHeight(8.0)),

      ),
      backgroundColor: Colors.white,
      body:  ListView(
        children: [
          SizedBox(
            height: 30,
          ),
          Container(
              margin: const EdgeInsets.all(6),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                child: Center(

                  child:
                  ListTile(
                    title: Text(translator.translate("Select Language"),
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'Righteous',
                            fontSize: 17)),
                  ),
                ),
              )
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            margin: const EdgeInsets.all(6),
            child:
            InkWell(
                onTap: (){

                },
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        leading:Image.asset(

                            "assets/images/united-states.png"
                        ),
                        title: Text(translator.translate("English"),
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Righteous',
                                fontSize: 17)),
                      ),

                    ],
                  ),
                )
            ),
          ),
          Container(
            margin: const EdgeInsets.all(6),
            child:
            InkWell(
                onTap: (){

                },
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        leading:Image.asset(

                            "assets/images/pakistan.png"
                        ),
                        title: Text(translator.translate("Urdu"),
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Righteous',
                                fontSize: 17)),
                      ),

                    ],
                  ),
                )
            ),
          ),

          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
  void showAll() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              width: 260.0,
              height: 200.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(60.0)),
              ),
              child: Column(children: [
                Row(children: [
                  Radio(
                    value: 1,
                    groupValue: typeId,
                    onChanged: (val) {
                      setState(() {
                        radioButtonItem = 'All';

                        Navigator.of(context).pop();

                        typeId = 1;
                      });
                    },
                  ),
                  Text(
                    translator.translate('All'),
                    style: new TextStyle(fontSize: 14.0),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 2,
                    groupValue: typeId,
                    onChanged: (val) {
                      setState(() {
                        radioButtonItem = 'Current Location';

                        Navigator.of(context).pop();
                        typeId = 2;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Current Location'),
                    style: new TextStyle(
                      fontSize: 14.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 3,
                    groupValue: typeId,
                    onChanged: (val) {
                      setState(() {
                        radioButtonItem = 'Today';

                        Navigator.of(context).pop();
                        typeId = 3;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Today'),
                    style: new TextStyle(
                      fontSize: 14.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 4,
                    groupValue: typeId,
                    onChanged: (val) {
                      setState(() {
                        radioButtonItem = 'In 48 hours';

                        Navigator.of(context).pop();
                        typeId = 4;
                      });
                    },
                  ),
                  Text(
                    translator.translate('In 48 hours'),
                    style: new TextStyle(
                      fontSize: 14.0,
                    ),
                  ),
                ]),
              ]),
            ),
            backgroundColor: Colors.white,
            actions: [
              FlatButton(
                child: Text(translator.translate('Cancel')),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });

//
  }

  void showSort() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              width: 260.0,
              height: 200.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(60.0)),
              ),
              child: Column(children: [
                Row(children: [
                  Radio(
                    value: 1,
                    groupValue: allId,
                    onChanged: (val) {
                      setState(() {
                        radioButton = 'Date';

                        Navigator.of(context).pop();
                        allId = 1;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Date'),
                    style: new TextStyle(fontSize: 14.0),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 2,
                    groupValue: allId,
                    onChanged: (val) {
                      setState(() {
                        radioButton = 'Price low to high';

                        Navigator.of(context).pop();
                        allId = 2;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Price low to high'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),
                Row(children: [
                  Radio(
                    value: 3,
                    groupValue: allId,
                    onChanged: (val) {
                      setState(() {
                        radioButton = 'Price high to low';

                        Navigator.of(context).pop();
                        allId = 3;
                      });
                    },
                  ),
                  Text(
                    translator.translate('Price high to low'),
                    style: new TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                ]),

              ]),
            ),
            backgroundColor: Colors.white,
            actions: [
              FlatButton(
                child: Text(translator.translate('Cancel')),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });

//
  }

}
