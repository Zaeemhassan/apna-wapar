import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'dart:io';
import 'package:video_player/video_player.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flick_video_player/flick_video_player.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:video_player_controls/video_player_controls.dart';

class Help extends StatefulWidget {
  var videoUrl;
  @override
  _HelpState createState() => new _HelpState();
  Help({
    Key key,
    @required this.videoUrl,
  }) : super(key: key);
}

class _HelpState extends State<Help> {
var helpingVideo;
bool isLoading = false;
  final FirebaseAuth auth = FirebaseAuth.instance;
  BannerAd _bannerAd;
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo();
  BannerAd createBannerAd() {
    return new BannerAd(
        adUnitId: "ca-app-pub-9603407988877715/3498665226",
        size: AdSize.banner,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("Banner event : $event");
        });
  }
  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
        adUnitId: "ca-app-pub-9603407988877715/8189731030",
        //Change Interstitial AdUnitId with Admob ID
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("IntersttialAd $event");
        });
  }
  FlickManager flickManager;
Controller controller;
  @override
  void initState() {

    super.initState();
    flickManager =  FlickManager(
        videoPlayerController: VideoPlayerController.network(widget.videoUrl));
    FirebaseAdMob.instance
        .initialize(appId: "ca-app-pub-9603407988877715~1895785423");
    _bannerAd = createBannerAd()
      ..load()
      ..show();
    controller = new Controller(
      items: [
        new PlayerItem(
          aspectRatio: 20 / 35,
          url: widget.videoUrl,

        ),

      ],
      autoPlay: true,
      errorBuilder: (context, message) {
        return new Container(
          child: new Text(message),
        );
      },
      // index: 2,
      autoInitialize: true,
      
      // isLooping: false,
      allowedScreenSleep: true,
      // showControls: false,
      // hasSubtitles: true,
      // isLive: true,
      // showSeekButtons: false,
      // showSkipButtons: false,
      // allowFullScreen: false,
      fullScreenByDefault: false,
      placeholder: new Container(
        color: Colors.grey,
      ),
      isPlaying: (isPlaying) {
        //
        // print(isPlaying);
      },

      playerItem: (playerItem) {
        // print('Player title: ' + playerItem.title);
        // print('position: ' + playerItem.position.inSeconds.toString());
        // print('Duration: ' + playerItem.duration.inSeconds.toString());
      },
      videosCompleted: (isCompleted) {
        print(isCompleted);
      },
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bannerAd?.dispose();
  }
  @override
  Widget build(BuildContext context) {
    TextStyle defaultStyle =
    TextStyle(color: Colors.black, fontSize: 14.0, fontFamily: 'Righteous');
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[500],
        title: Text(
          translator.translate('Help'),
          style: TextStyle(
              fontSize: 18, fontFamily: 'Righteous', color: Colors.white),
        ),
        bottom: PreferredSize(
            child: Container(
              color: Colors.black87,
              height: 8.0,
            ),
            preferredSize: Size.fromHeight(8.0)),

      ),
      backgroundColor: Colors.white,
      body:  ListView(
        children: [
          SizedBox(
            height: 20,
          ),

          Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: InkWell(
                  child:
                  Card(
                elevation: 1,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:[
                      ListTile(
                        title: Center(
                          child:
                          Text(translator.translate("If you need any help related to the app kindly see this video"),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Righteous',
                                  fontSize: 15)),
                        ),
                      )
                    ]
                ),
              )
              )
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 40),
            child:ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child:  AspectRatio(
                    aspectRatio: 20 / 35,
                    child:VideoPlayerControls(
                      controller: controller,
                    ),
                )

                )

            ),

          Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Card(
                elevation: 1,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:[
                      ListTile(
                        title: Center(
                          child:
                          Text(translator.translate("If you have any questions. Feel free to contact"),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Righteous',
                                  fontSize: 15)),
                        ),
                      )
                    ]
                ),
              )
          ),

          Container(
            margin: const EdgeInsets.fromLTRB(40, 0, 40, 0),
            child:
            Divider(
              thickness: 3,
              color: Colors.green,
            ),
          ),

          Container(
              margin: const EdgeInsets.fromLTRB(100, 0, 100, 0),
              child: Card(
                elevation: 1,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                  children:[
                  ListTile(
                    title: Center(
                child:
                Text(translator.translate("Our Email"),
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'Righteous',
                            fontSize: 15)),
                  ),
                  )
      ]
                ),
              )
          ),
          Container(
              margin: const EdgeInsets.fromLTRB(10,0, 10, 0),
              child: Card(
                elevation: 3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                child: Center(

                  child:
                  ListTile(
                    leading: Icon(Icons.email_rounded),
                    title:Row(
                      children: [
                        Text(translator.translate("Email : "),style: TextStyle(fontSize: 15,fontFamily: 'Righteous'),),
                    SelectableLinkify(
                      text: "saadlashari.sk@gmail.com",
                      onTap:  () {
                        createInterstitialAd()
                          ..load()
                          ..show();
                        launch(
                            "mailto:saadlashari.sk@gmail.com");
                      }
                    )
                      ],
                    )
                  ),
                ),
              )
          ),
          Container(
              margin: const EdgeInsets.fromLTRB(80, 0, 80, 0),
              child: Card(
                elevation: 1,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:[
                      ListTile(
                        title: Center(
                          child:
                          Text(translator.translate("Our PhoneNumber"),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'Righteous',
                                  fontSize: 15)),
                        ),
                      )
                    ]
                ),
              )
          ),
          Container(
              margin: const EdgeInsets.fromLTRB(10,0, 10, 0),
              child: Card(
                elevation: 3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                child: Center(

                  child:
                  ListTile(
                      leading: Icon(Icons.phone_android),
                      title:Row(
                        children: [
                          Text(translator.translate("Phone Number : "),style: TextStyle(fontSize: 15,fontFamily: 'Righteous'),),
                          SelectableLinkify(
                            text: "03038146045",
                            onTap:  () {
                              createInterstitialAd()
                                ..load()
                                ..show();
                              launch(
                                  "tel:03038146045");

                            }
                          )

                        ],
                      )
                  ),
                ),
              )
          ),
          SizedBox(
            height: 20,
          ),

        ],
      ),
    );
  }
}
