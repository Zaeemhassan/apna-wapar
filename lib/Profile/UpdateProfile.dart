import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:toast/toast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:apna_beepar/Profile/UserProfile.dart';
import 'package:localize_and_translate/localize_and_translate.dart';

class UpdateUserProfile extends StatefulWidget {
  var updateUserName;
  var updateUserContact;
  var documentId;
  var updateUserAddress;
  @override
  _UpdateUserProfileState createState() => _UpdateUserProfileState();
  UpdateUserProfile(
      {Key key,
      @required this.updateUserName,
      @required this.updateUserContact,
      @required this.documentId,
      @required this.updateUserAddress})
      : super(key: key);
}

class _UpdateUserProfileState extends State<UpdateUserProfile> {
  bool isVisible = false;
  var isLoading = true;
  var userId;
  var updateUserProfileName;
  var updateUserAddress;
  var updateUserPhoneNumber;

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  final FirebaseAuth auth = FirebaseAuth.instance;

  Future updateUserProfile() async {
    Firestore.instance
        .collection("users")
        .document(widget.documentId)
        .updateData({
      "name": updateUserProfileName,
      "Address": updateUserAddress,
      "PhoneNumber": updateUserPhoneNumber
    }).then((value) {});
//
  }

  @override
  void initState() {
    super.initState();
    print("document id ${widget.documentId}");
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightGreen[500],
          title: Text(
            translator.translate('Update User Profile'),
            style: TextStyle(
                fontSize: 18, fontFamily: 'Righteous', color: Colors.white),
          ),
          bottom: PreferredSize(
              child: Container(
                color: Colors.black87,
                height: 8.0,
              ),
              preferredSize: Size.fromHeight(8.0)),

        ),
        floatingActionButton: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  setState(() {
                    isLoading = true;
                  });

                  updateUserProfile();
                }

                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => UserProfile()));
                Toast.show(
                    translator.translate('Your profile has updated'),
                    context,
                    duration: 3);
              },
              icon: Icon(
                Icons.update,
                color: Colors.white,
              ),
              label: Text(
                translator.translate('Update User Profile'),
                style: TextStyle(fontFamily: 'Righteous', color: Colors.white),
              ),
              backgroundColor: Colors.lightGreen[600],
            ),
          ],
        ),
        backgroundColor: Colors.white,
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Container(
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.all(10),
                  child: TextFormField(
                    initialValue: " ${widget.updateUserName}",
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: translator.translate("Update User Name"),
                      hintText: translator.translate("Update User Name"),
                      prefixIcon: Icon(
                        Icons.title,
                        color: Colors.lightGreen[400],
                      ),
                      fillColor: Colors.white,
                      border: new OutlineInputBorder(
                        borderSide: new BorderSide(),
                      ),
                      //fillColor: Colors.green
                    ),
                    onSaved: (String value) {
                      updateUserProfileName = value;
                    },
                    validator: (String value) {
                      if (value.isEmpty) {
                        return translator.translate('Enter your Name');
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.all(10),
                  child: TextFormField(
                    initialValue: " ${widget.updateUserContact}",
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText:
                          translator.translate("Update User PhoneNumber"),
                      hintText: "+9232.......",
                      prefixIcon: Icon(
                        Icons.phone_android,
                        color: Colors.lightGreen[400],
                      ),
                      fillColor: Colors.white,
                      border: new OutlineInputBorder(
                        borderSide: new BorderSide(),
                      ),
                      //fillColor: Colors.green
                    ),
                    onSaved: (String value) {
                      updateUserPhoneNumber = value;
                    },
                    validator: (String value) {
                      if (value.isEmpty) {
                        return translator.translate('Enter your phone number');
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.all(10),
                  child: TextFormField(
                    initialValue: " ${widget.updateUserAddress}",
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: translator.translate("Update User Address"),
                      hintText: translator.translate("Update User Address"),
                      prefixIcon: Icon(
                        Icons.home_outlined,
                        color: Colors.lightGreen[400],
                      ),
                      fillColor: Colors.white,
                      border: new OutlineInputBorder(
                        borderSide: new BorderSide(),
                      ),
                      //fillColor: Colors.green
                    ),
                    onSaved: (String value) {
                      updateUserAddress = value;
                    },
                    validator: (String value) {
                      if (value.isEmpty) {
                        return translator.translate('Enter your address');
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ),


      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
