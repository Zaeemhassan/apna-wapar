import 'package:apna_beepar/SplashScreen.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:apna_beepar/UserHomePage/HomePage.dart';
import 'package:apna_beepar/Profile/UpdateProfile.dart';
import 'package:localize_and_translate/localize_and_translate.dart';

class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => new _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  var userProfileName;
  var userProfileAddress;
  var userProfilePhone;
  double userProfileRating;
  var userProfileCount;
  var userId;
  double totalRatingWithOnlyOneDecimal;
  String totalRating;
  bool dataCheck = false;
  bool isLoading = false;
  var document;
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  final FirebaseAuth auth = FirebaseAuth.instance;

  void _signOut() async {
    await auth.signOut();
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    await Future.delayed(Duration(seconds: 1));

    Navigator.of(context).pushAndRemoveUntil(
      // the new route
      MaterialPageRoute(
        builder: (BuildContext context) => SplashScreen(),
      ),

          (Route route) => false,
    );
  }

  var userProfileNumber;

  Future getUserProfile() async {
    setState(() {
      isLoading = true;
    });
    var firebaseUser = await FirebaseAuth.instance.currentUser();
    userId = firebaseUser.uid;
    Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .get()
        .then((value) {
      print(value.data);
      var id = value.documentID;
      document = (id);
      var user = value.data;
      print("user $user");
      userProfileName = (user['name']);
      userProfileAddress = (user['Address']);
      userProfilePhone = (user["PhoneNumber"]);
      userProfileRating = (user['rating']);
      userProfileCount = user['count'];
      if (user.isNotEmpty) {
        setState(() {
          dataCheck = true;
        });
      }
      setState(() {
        isLoading = false;
      });
      totalRatingWithOnlyOneDecimal = userProfileRating / userProfileCount;
      totalRating = totalRatingWithOnlyOneDecimal.toStringAsFixed(1);
    });
    print("rating $userProfileCount");
  }
  BannerAd _bannerAd;
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo();
  BannerAd createBannerAd() {
    return new BannerAd(
        adUnitId: "ca-app-pub-9179231796573696/7855388181",
        size: AdSize.banner,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("Banner event : $event");
        });
  }
  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
        adUnitId: "ca-app-pub-9603407988877715/8189731030",
        //Change Interstitial AdUnitId with Admob ID
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("IntersttialAd $event");
        });
  }
//
  @override
  void initState() {
    super.initState();
    getUserProfile();
    FirebaseAdMob.instance
        .initialize(appId: "ca-app-pub-9179231796573696~7800930750");
    _bannerAd = createBannerAd()
      ..load()
      ..show();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bannerAd?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[500],
        title: Text(
          translator.translate('Profile'),
          style: TextStyle(
              fontSize: 18, fontFamily: 'Righteous', color: Colors.white),
        ),
        bottom: PreferredSize(
            child: Container(
              color: Colors.black87,
              height: 8.0,
            ),
            preferredSize: Size.fromHeight(8.0)),
      ),
      backgroundColor: Colors.white,
      body: isLoading
          ? SpinKitDoubleBounce(
        color: Colors.greenAccent[700],
      )
          : ListView(
        children:dataCheck
            ?<Widget> [
          SizedBox(
            height: 30,
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(40, 0, 40, 0),
            child: Column(children: [
              Center(
                  child: SizedBox(
                      height: 250,
                      width: 250,
                      child: Image.asset("assets/images/profile.png"))),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,

                  children:
                  [_profileRating(),
                    Icon(
                      Icons.star,
                      color: Colors.yellow[700],
                    ),
                    Text(
                      "(" + "$userProfileCount" + ")",
                      style: TextStyle(
                          fontFamily: 'Righteous', fontSize: 16),
                    ),
                  ])

            ]),
          ),
          Container(
            margin: const EdgeInsets.all(6),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    title: Text(
                        translator.translate("Name : ") + userProfileName,
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'Righteous',
                            fontSize: 17)),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.all(6),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    title: Text(translator.translate("PhoneNumber : ") +
                        userProfilePhone,
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'Righteous',
                            fontSize: 17)),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.all(6),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    title: Text(
                        translator.translate("Address : ") + userProfileAddress,
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'Righteous',
                            fontSize: 17)),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
              padding: EdgeInsets.fromLTRB(25.0, 8, 25.0, 8),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: SizedBox(
                      width: 60,
                      height: 60,
                      child: FloatingActionButton(
                        heroTag: null,
                        onPressed: () {
                          createInterstitialAd()
                            ..load()
                            ..show();
                          Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) =>
                                    UpdateUserProfile(
                                      documentId: document,
                                      updateUserName: userProfileName,
                                      updateUserAddress:
                                      userProfileAddress,
                                      updateUserContact: userProfilePhone,
                                    )),
                          );
                        },
                        child: new Icon(
                          CupertinoIcons.shuffle_medium,
                          size: 30,
                          color: Colors.lightGreen[600],
                        ),
                        backgroundColor: Colors.white,
                      ),
                    ),
                  ),
                  Expanded(
                      child: SizedBox(
                        height: 60,
                        width: 60,
                        child: FloatingActionButton(
                          heroTag: null,
                          onPressed: () {
                            createInterstitialAd()
                              ..load()
                              ..show();
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    content: Text(
                                      translator.translate(
                                          'Are you sure you want to sign out?'),
                                      style: TextStyle(
                                          fontFamily: 'Righteous',
                                          color: Colors.black),
                                    ),
                                    backgroundColor: Colors.lightGreen[100],
                                    actions: [
                                      FlatButton(
                                        child:
                                        Text(translator.translate('Yes')),
                                        onPressed: () async {
                                          _signOut();
                                        },
                                      ),
                                      FlatButton(
                                        child:
                                        Text(translator.translate('No')),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                });
//
                          },
                          child: new Icon(
                            Icons.power_settings_new,
                            size: 30,
                            color: Colors.lightGreen[600],
                          ),
                          backgroundColor: Colors.white,
                        ),
                      )),
                ],
              )),
          SizedBox(
            height: 20,
          )
        ]: <Widget>[
          Container(
              padding: EdgeInsets.fromLTRB(
                  50.0, 100.0, 50.0, 50.0),
              child: Center(
                child: Text(
                  "No data available",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Righteous',
                    //decoration: TextDecoration.none
                  ),
                ),
              ))
        ]
      ),
    );
  }

  Widget _profileRating() {
    if (userProfileRating == 0.0) {
      return Text(
        userProfileRating.toString(),
        style: TextStyle(fontFamily: 'Righteous', fontSize: 16),
      );
    }
    else {
      return Text(
        totalRating.toString(),
        style: TextStyle(fontFamily: 'Righteous', fontSize: 16),
      );
    }
  }
}
