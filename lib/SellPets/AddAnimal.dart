import 'package:apna_beepar/SellPets/SellAnimalsCategory.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flick_video_player/flick_video_player.dart';
import 'package:video_player/video_player.dart';
import 'package:localize_and_translate/localize_and_translate.dart';

class AddAnimalForSell extends StatefulWidget {
  final String animalCategoryName;
  final String currentAddress;
  @override
  _AddAnimalForSellState createState() => _AddAnimalForSellState();
  AddAnimalForSell(
      {Key key,
      @required this.animalCategoryName,
      @required this.currentAddress})
      : super(key: key);
}

class _AddAnimalForSellState extends State<AddAnimalForSell> {
  bool isVisible = false;
  bool isLoading = false;
  var animalBreed;
  var animalPrice;
  var animalDescription;
  var createDate;
  File _image;
  File _image2;
  File _image3;
  File _video;
  var animalImage1;
  var animalImage2;
  var animalImage3;
  var userId;
  var animalVideo;
  var animalCity;
  var selectedPregnancy, selectedType;
  var pregnanciesCount = [
    translator.translate('Never'),
    translator.translate('1 Time'),
    translator.translate('2 Times'),
    translator.translate('3 Times'),
    translator.translate('4 Times'),
    translator.translate('5 Times'),
    translator.translate('6 Times'),
  translator.translate('7 Times'),
  translator.translate('8 Times'),
  translator.translate('9 Times'),
  translator.translate('10 Times'),
  translator.translate('More than 10 Times')
  ];
  var selectedPregnancyStatus, selectedTypeStatus;
  var pregnanciesStatus = [
    translator.translate('Not Pregnant'),
    translator.translate('First Month'),
    translator.translate('Second Month'),
    translator.translate('Third Month'),
    translator.translate('Fourth Month'),
    translator.translate('Fifth Month'),
    translator.translate('Sixth Month'),
    translator.translate('Seventh Month'),
    translator.translate('Eighth Month'),
    translator.translate('Ninth Month'),
    translator.translate('Tenth Month')
  ];
  var selectedGender, selectedTypeGender;
  var animalGender = [
    translator.translate('Male'),
    translator.translate("Female")
  ];
  TargetPlatform _platform;
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;
  var currentDateTime;
  String formattedDate;
  Future getCurrentUser() async {
    var firebaseUser = await FirebaseAuth.instance.currentUser();
    userId = firebaseUser.uid;
    print("ID $userId");
    Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .get()
        .then((value) {
      setState(() {
        isLoading = true;
        print(value.data);
      });
    });
  }

  var userProfilePhone;
  var userProfileName;
  var userProfileAddress;

  Future getUserProfile() async {
    setState(() {
      isLoading = true;
    });
    var firebaseUser = await FirebaseAuth.instance.currentUser();
    userId = firebaseUser.uid;
    Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .get()
        .then((value) {
      print(value.data);
      var id = value.documentID;

      var user = value.data;

      userProfileName = (user['name']);
      userProfileAddress = (user['Address']);
      userProfilePhone = (user["PhoneNumber"]);
      setState(() {
        isLoading = false;
      });
    });
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
    String fileName = basename(_image.path);
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('assets/animal/images/$fileName');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    animalImage1 = await firebaseStorageRef.getDownloadURL() as String;
    setState(() {
      print("Profile Picture uploaded");
    });
  }
  FlickManager flickManager;
  Future getVideo() async {
    var video = await ImagePicker.pickVideo(source: ImageSource.gallery);

    setState(() {
      _video = video;
    });
    String fileName = basename(_video.path);
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('assets/animal/videos/$fileName');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(
        _video, StorageMetadata(contentType: 'video/mp4'));
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    animalVideo = await firebaseStorageRef.getDownloadURL() as String;
    setState(() {
      print("Profile Picture uploaded");
    });
  }
  Future getImage2() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image2 = image;
    });
    String fileName = basename(_image2.path);
    StorageReference firebaseStorageRef =
    FirebaseStorage.instance.ref().child('assets/animal/images/$fileName');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image2);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    animalImage2 = await firebaseStorageRef.getDownloadURL() as String;
    setState(() {
      print("Profile Picture uploaded");
    });
  }
  Future getImage3() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image3 = image;
    });
    String fileName = basename(_image3.path);
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('assets/animal/images/$fileName');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image3);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    animalImage3 = await firebaseStorageRef.getDownloadURL() as String;
    setState(() {
      print("Profile Picture uploaded");
    });
  }

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  final FirebaseAuth auth = FirebaseAuth.instance;

  Future addAnimalForSell() async {

    Firestore.instance.collection("SellAnimalList").add({
      "SellAnimalList": {
        "UserName": userProfileName,
        "UserAddress": userProfileAddress,
        "UserPhoneNumber": userProfilePhone,
        "UserId": userId,
        "AnimalBreed": animalBreed ?? "No data",
        "AnimalCategoryName": widget.animalCategoryName,
        "AnimalImage1": animalImage1,
        "AnimalImage2": animalImage2,
        "AnimalImage3": animalImage3,
        "AnimalVideo": animalVideo ?? "No data",
        "AnimalPrice": int.parse(animalPrice),
        "AnimalPregnancyCount": selectedType ?? "No data",
        "AnimalPregnancyStatus": selectedTypeStatus ?? "No data",
        "AnimalGender": selectedTypeGender,
        "AnimalDescription": animalDescription ?? "No data",
        "AnimalLocation": widget.currentAddress,
        "CurrentDate": currentDate,
        "CurrentTime": currentTime,
        "DateTime": currentDateTime,
        "AnimalCity": animalCity
      },
    }).then((value) {
      print(value.documentID);
      print(value);
    });
//
  }

  var currentDate;
  var currentTime;
  var now;
  @override
  void initState() {
    super.initState();
    getUserProfile();
    getCurrentUser();

  }

  @override
  void dispose() {
    flickManager.dispose();
    super.dispose();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightGreen[500],
          title: Text(
            translator.translate('Add Animal Details For Sale'),
            style: TextStyle(
                fontSize: 16, fontFamily: 'Righteous', color: Colors.white),
          ),
          bottom: PreferredSize(
              child: Container(
                color: Colors.black87,
                height: 8.0,
              ),
              preferredSize: Size.fromHeight(8.0)),
        ),
        floatingActionButton: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  setState(() {
                    isLoading = true;
                  });
                  DateTime now = new DateTime.now();
                  formattedDate = DateFormat('dd-MM-yyyy h:mma').format(now);
                  currentDateTime = formattedDate;
                  addAnimalForSell();

                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ViewAnimalCategory()));
                }
              },
              icon: Icon(
                Icons.send,
                color: Colors.white,
              ),
              label: Text(
                translator.translate('Add'),
                style: TextStyle(fontFamily: 'Righteous', color: Colors.white),
              ),
              backgroundColor: Colors.lightGreen[600],
            ),
          ],
        ),
        backgroundColor: Colors.white,

        body: ListView(children: [
          Container(
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: Text(
                        translator.translate('Add Images'),
                        style: TextStyle(
                            fontSize: 16, fontFamily: 'Righteous', color: Colors.black),
                      ),
                    ),

                    Container(

                      margin: const EdgeInsets.all(10),
                      height: 100,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                           child:Container(
                             decoration: BoxDecoration(
                               border: Border.all(
                                color: Colors.green
                               ),
                             ),
                            child: InkWell(
                              onTap: () {
                                getImage();
                              },
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: (_image != null)
                                    ? Image.file(
                                        _image,
                                        width: 40,
                                        height: 70,
                                      )
                                    : Icon(
                                        FontAwesomeIcons.camera,
                                        size: 20.0,
                                      ),
                              ),
                            ),
                          ),
                          ),
                          Expanded(
                            child:Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.green
                                ),
                              ),
                              child: InkWell(
                                onTap: () {
                                  getImage2();
                                },
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(8.0),
                                  child: (_image2 != null)
                                      ? Image.file(
                                    _image2,
                                    width: 40,
                                    height: 70,
                                  )
                                      : Icon(
                                    FontAwesomeIcons.camera,
                                    size: 20.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child:Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.green
                                ),
                              ),
                              child: InkWell(
                                onTap: () {
                                  getImage3();
                                },
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(8.0),
                                  child: (_image3 != null)
                                      ? Image.file(
                                    _image3,
                                    width: 40,
                                    height: 70,
                                  )
                                      : Icon(
                                    FontAwesomeIcons.camera,
                                    size: 20.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: Text(
                        translator.translate('Add Video'),
                        style: TextStyle(
                            fontSize: 16, fontFamily: 'Righteous', color: Colors.black),
                      ),
                    ),
                    Container(
                      height: 200,

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Align(
                              alignment: Alignment.center,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: (_video != null)
                                    ? AspectRatio(
                                    aspectRatio: 23 / 17,
                                    child: FlickVideoPlayer(
                                      flickManager: FlickManager(
                                          videoPlayerController: VideoPlayerController.file(_video)),
                                      flickVideoWithControls: FlickVideoWithControls(
                                        controls: FlickPortraitControls(),
                                      ),
                                    )
                                )
                                    :  Image.asset(
                                  "assets/images/loading.gif",
                                  fit: BoxFit.fill,
                                ),

                              ),),
                          Padding(
                            padding: EdgeInsets.only(top: 30.0),
                            child: IconButton(
                              icon: Icon(
                                FontAwesomeIcons.camera,
                                size: 30.0,
                              ),
                              onPressed: () {
                                getVideo();
//                                    uploadPic(context);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Column(children: [
                      Container(
                        margin: EdgeInsets.all(10),
                        child: TextFormField(
                          enabled: false,
                          initialValue: widget.currentAddress,
                          decoration: InputDecoration(
                            labelText: translator.translate("Location"),
                            prefixIcon: Icon(
                              Icons.location_pin,
                              color: Colors.lightGreen[400],
                            ),

                            fillColor: Colors.white,
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(25.0),
                              borderSide: new BorderSide(),
                            ),
                            //fillColor: Colors.green
                          ),
                        ),
                      ),
                    ]),
                    SizedBox(height: 10.0),
                    Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.all(10),
                      child: TextFormField(
                        enabled: false,
                        initialValue: "${widget.animalCategoryName}",
                        decoration: InputDecoration(
                          labelText: translator.translate("Animal Category"),
                          prefixIcon: Icon(
                            Icons.category,
                            color: Colors.lightGreen[400],
                          ),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.all(10),
                      child: TextFormField(
                        inputFormatters: [new WhitelistingTextInputFormatter(RegExp("[a-zA-Z ]")),],
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          labelText: translator.translate("City"),
                          hintText: "City",
                          prefixIcon: Icon(
                            Icons.location_city,
                            color: Colors.lightGreen[400],
                          ),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return translator.translate('Enter your city');
                          }
                          return null;
                        },
                        onSaved: (value) {
                          animalCity = value;
                        },
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      // alignment: Alignment.topCenter,
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: DropdownButton(
                          items: animalGender
                              .map((value) => DropdownMenuItem(
                            child: Text(
                              value,
                              style: TextStyle(color: Colors.black),
                            ),
                            value: value,
                          ))
                              .toList(),
                          onChanged: (selectedGender) {
                            setState(() {
                              selectedTypeGender = selectedGender;
                            });
                          },
                          value: selectedTypeGender,
                          isExpanded: false,
                          hint: Text(
                            translator.translate('Gender'),
                            style: TextStyle(color: Colors.lightGreen[500]),
                          ),
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    selectedTypeGender == "Male"
                    ?Container():
                    Container(
                        // alignment: Alignment.topCenter,
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: DropdownButton(
                          items: pregnanciesCount
                              .map((value) => DropdownMenuItem(
                                    child: Text(
                                      value,
                                      style: TextStyle(color: Colors.black),
                                    ),
                                    value: value,
                                  ))
                              .toList(),
                          onChanged: (selectedPregnancy) {
                            setState(() {
                              selectedType = selectedPregnancy;
                            });
                          },
                          value: selectedType,
                          isExpanded: false,
                          hint: Text(
                            translator.translate('Select Pregnancy Count'),
                            style: TextStyle(color: Colors.lightGreen[500]),
                          ),
                        )),
                    selectedTypeGender == "Male"
                        ?Container():
                    Container(
                        // alignment: Alignment.topCenter,
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: DropdownButton(
                          items: pregnanciesStatus
                              .map((value) => DropdownMenuItem(
                                    child: Text(
                                      value,
                                      style: TextStyle(color: Colors.black),
                                    ),
                                    value: value,
                                  ))
                              .toList(),
                          onChanged: (selectedPregnancyStatus) {
                            setState(() {
                              selectedTypeStatus = selectedPregnancyStatus;
                            });
                          },
                          value: selectedTypeStatus,
                          isExpanded: false,
                          hint: Text(
                            translator.translate('Select Pregnancy Status'),
                            style: TextStyle(color: Colors.lightGreen[500]),
                          ),
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.all(10),
                      child: TextFormField(
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                          labelText: translator.translate("Breed"),
                          hintText: translator.translate("Breed"),
                          prefixIcon: Icon(
                            Icons.text_format_outlined,
                            color: Colors.lightGreen[400],
                          ),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),
                        onSaved: (value) {
                          animalBreed = value;
                        },
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.all(10),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: translator.translate("Price"),
                          hintText: "123",
                          prefixIcon: Icon(
                            Icons.attach_money,
                            color: Colors.lightGreen[400],
                          ),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return translator.translate('Enter your animal price');
                          }
                          return null;
                        },
                        onSaved: (value) {
                          animalPrice = value;
                        },
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.all(10),
                      child: TextFormField(
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                          labelText: translator.translate("Description"),
                          hintText: translator.translate("Description"),
                          prefixIcon: Icon(
                            Icons.note,
                            color: Colors.lightGreen[400],
                          ),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),
                        onSaved: (value) {
                          animalDescription = value;
                        },
                      ),
                    ),
                    SizedBox(height: 10.0),
                  ],
                ),
              ),
            ),
          ),
        ]));
  }
}
