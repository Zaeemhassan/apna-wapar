import 'package:apna_beepar/SellPets/AddAnimal.dart';
import 'package:apna_beepar/UserHomePage/HomePage.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:localize_and_translate/localize_and_translate.dart';

class ViewAnimalCategory extends StatefulWidget {
  @override
  _ViewAnimalCategoryState createState() => new _ViewAnimalCategoryState();
}

class _ViewAnimalCategoryState extends State<ViewAnimalCategory> {
  bool isLoading = true;
  var id;
  var categoryProduct;
  var documnet = [];
  List itemsData = [];
  var animalName = [];
  var animalImage = [];
  List document = [];
  var deleteCategoryData;
  bool dataCheck = false;
  String _currentAddress;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  _getCurrentLocation() {
    setState(() {
      isLoading=true;
    });
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
      setState(() {
        isLoading=false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
        "${place.locality}, ${place.country}";
      });
      print('address $_currentAddress');
    } catch (e) {
      print(e);
    }
  }
  Future getMenu() async {
   setState(() {
     isLoading = true;
   });
   Firestore.instance
       .collection("Animal Category")
       .getDocuments()
       .then((querySnapshot) {
     querySnapshot.documents.forEach((result) {
       id = result.documentID;
       document.add(id);
       var menu = (result.data["Animal Category"]);

//        print("data $category");
       animalName.add(menu['AnimalName']);
       animalImage.add(menu['AnimalImage']);
        if (menu.isNotEmpty) {
          setState(() {
            dataCheck = true;
          });
        }
      });

      setState(() {
        isLoading = false;
      });
    });
  }

  Future deleteCategory(deleteCategoryData) async {
    Firestore.instance
        .collection("Animal Category")
        .document(deleteCategoryData)
        .delete()
        .then((_) {});
  }
  BannerAd _bannerAd;
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo();
  BannerAd createBannerAd() {
    return new BannerAd(
        adUnitId: "ca-app-pub-9603407988877715/3498665226",
        size: AdSize.banner,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("Banner event : $event");
        });
  }
  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
        adUnitId: "ca-app-pub-9603407988877715/8189731030",
        //Change Interstitial AdUnitId with Admob ID
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("IntersttialAd $event");
        });
  }
  @override
  void initState() {
    FirebaseAdMob.instance
        .initialize(appId: "ca-app-pub-9603407988877715~1895785423");
    _bannerAd = createBannerAd()
      ..load()
      ..show();
    getMenu();
    _getCurrentLocation();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bannerAd?.dispose();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.lightGreen[500],
              title: Text(
                translator.translate('Select Animal Category'),
                style: TextStyle(
                    fontSize: 18, fontFamily: 'Righteous', color: Colors.white),
              ),
              bottom: PreferredSize(
                  child: Container(
                    color: Colors.black87,
                    height: 8.0,
                  ),
                  preferredSize: Size.fromHeight(8.0)),
            ),
            backgroundColor: Colors.white,
            body: isLoading
                ? SpinKitDoubleBounce(
              color: Colors.greenAccent[700],
            )
                : Center(
              child: ListView(
                  children:dataCheck
                      ?<Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: GridView.builder(
                          shrinkWrap: true,
                          physics: BouncingScrollPhysics(),
                          itemCount: animalName?.length ?? 0,
                          gridDelegate:
                          SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2),
                          itemBuilder: (context, index) => new InkWell(
                            onTap: () async{
                              createInterstitialAd()
                                ..load()
                                ..show();
                              Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) => AddAnimalForSell(
                                      animalCategoryName:animalName[index],
                                      currentAddress: _currentAddress,

                                    )),
                              );
                            },
                            child: Card(
                              elevation: 8,
                              semanticContainer: true,
                              color: Colors.white,
                              child: Container(
//                            width: 100.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.elliptical(60.0, 60.0)),
//                                  color: Colors.indigo[200],
                                ),
                                child: Column(

                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [

                                      ClipRRect(
                                        borderRadius:
                                        BorderRadius.circular(15.0),
                                        child: Image.network(
                                          animalImage[index].toString(),
                                          height: 80.0,
                                          width: 140.0,
                                          fit: BoxFit.fill,
                                        ),
                                      ),

                                      Text(
                                        animalName[index].toString(),
                                        style: TextStyle(
                                            fontFamily: "Righteous",
                                            fontSize: 20),
                                      ),
                                    ]),
                              ),
                            ),
                          )),
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ]
                      : <Widget>[
                    Container(
                        padding: EdgeInsets.fromLTRB(
                            50.0, 100.0, 50.0, 50.0),
                        child: Center(
                          child: Text(
                            translator.translate("No data available"),
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 25.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Righteous',
                              //decoration: TextDecoration.none
                            ),
                          ),
                        ))
                  ]
              ),
            )
    );
  }
}
