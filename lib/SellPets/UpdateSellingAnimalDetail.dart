import 'package:apna_beepar/SellPets/SellAnimalsCategory.dart';
import 'package:apna_beepar/SellPets/ViewPersonalSellingList.dart';
import 'package:apna_beepar/UserHomePage/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/services.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:flick_video_player/flick_video_player.dart';
import 'package:video_player/video_player.dart';

class UpdateAnimalDetails extends StatefulWidget {
  var animalPriceDetail;
  final String animalNameDetail;
  final String animalPregnancyStatus;
  final String animalPregnancyCount;
  var animalImage1Detail;
  var animalImage2Detail;
  var animalImage3Detail;
  var animalAllImages=[];
  final String animalVideoDetail;
  final String animalDescriptionDetail;
  final String animalLocationDetail;
  final String animalGenderDetail;
  final String animalBreedDetail;
  final String sellingDocumentId;
  var city;
  @override
  _UpdateAnimalDetailsState createState() => _UpdateAnimalDetailsState();
  UpdateAnimalDetails({Key key,
    @required this.animalPregnancyStatus,
    @required this.animalPregnancyCount,
    @required this.animalDescriptionDetail,
    @required this.animalLocationDetail,
    @required this.animalBreedDetail,
    @required this.animalAllImages,
    @required this.animalGenderDetail,
    @required this.animalPriceDetail,
    @required this.animalNameDetail,
    @required this.animalImage2Detail,
    @required this.animalImage1Detail,
    @required this.animalVideoDetail,
    @required this.animalImage3Detail, this.sellingDocumentId,
    @required this.city,

  }) : super(key: key);
}

class _UpdateAnimalDetailsState extends State<UpdateAnimalDetails> {

  bool isVisible = false;
  bool isLoading = false;
  var updateAnimalBreed;
  var updateAnimalPrice;
  var updateAnimalDescription;
  var createDate;
  File _image;
  File _image2;
  File _image3;
  File _video;
  var updateAnimalVideo;
  var updateAnimalImage1;
  var updateAnimalImage2;
  var updateAnimalImage3;
  var userId;
  var selectedPregnancy, selectedType;
  var pregnanciesCount = [
    translator.translate('Never'),
    translator.translate('1 Time'),
    translator.translate('2 Times'),
    translator.translate('3 Times'),
    translator.translate('4 Times'),
    translator.translate('5 Times'),
    translator.translate('6 Times'),
    translator.translate('7 Times'),
    translator.translate('8 Times'),
    translator.translate('9 Times'),
    translator.translate('10 Times'),
    translator.translate('More than 10 Times')
  ];
  var selectedPregnancyStatus, selectedTypeStatus;
  var pregnanciesStatus = [
    translator.translate('Not Pregnant'),
    translator.translate('First Month'),
    translator.translate('Second Month'),
    translator.translate('Third Month'),
    translator.translate('Fourth Month'),
    translator.translate('Fifth Month'),
    translator.translate('Sixth Month'),
    translator.translate('Seventh Month'),
    translator.translate('Eighth Month'),
    translator.translate('Ninth Month'),
    translator.translate('Tenth Month')
  ];
  var selectedGender,selectedTypeGender;
  var updateAnimalGender = [
    translator.translate('Male'),
    translator.translate("Female")
  ];
  Future getCurrentUser() async {
    var firebaseUser = await FirebaseAuth.instance.currentUser();
    userId = firebaseUser.uid;
    print("ID $userId");
    Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .get()
        .then((value) {
      setState(() {
        isLoading = true;
        print(value.data);
      });
    });
  }
  var userProfilePhone;
  var userProfileName;
  var userProfileAddress;

  Future getUserProfile() async {
    setState(() {
      isLoading = true;
    });
    var firebaseUser = await FirebaseAuth.instance.currentUser();
    userId = firebaseUser.uid;
    Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .get()
        .then((value) {
      print(value.data);
      var id = value.documentID;

      var user = value.data;

      userProfileName = (user['name']);
      userProfileAddress = (user['Address']);
      userProfilePhone = (user["PhoneNumber"]);
      setState(() {
        isLoading = false;
      });
    });
  }
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
    String fileName = basename(_image.path);
    StorageReference firebaseStorageRef = FirebaseStorage.instance
        .ref()
        .child('assets/animal/images/$fileName');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    updateAnimalImage1 = await firebaseStorageRef.getDownloadURL() as String;
    setState(() {
      print("Profile Picture uploaded");
    });
  }
  Future getImage2() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image2 = image;
    });
    String fileName = basename(_image2.path);
    StorageReference firebaseStorageRef = FirebaseStorage.instance
        .ref()
        .child('assets/animal/images/$fileName');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image2);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    updateAnimalImage2 = await firebaseStorageRef.getDownloadURL() as String;
    setState(() {
      print("Profile Picture uploaded");
    });
  }
  Future getImage3() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image3 = image;
    });
    String fileName = basename(_image3.path);
    StorageReference firebaseStorageRef = FirebaseStorage.instance
        .ref()
        .child('assets/animal/images/$fileName');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image3);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    updateAnimalImage3 = await firebaseStorageRef.getDownloadURL() as String;
    setState(() {
      print("Profile Picture uploaded");
    });
  }
  FlickManager flickManager;
  Future getVideo() async {
    var video = await ImagePicker.pickVideo(source: ImageSource.gallery);

    setState(() {
      _video = video;
    });
    String fileName = basename(_video.path);
    StorageReference firebaseStorageRef =
    FirebaseStorage.instance.ref().child('assets/animal/videos/$fileName');
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(
        _video, StorageMetadata(contentType: 'video/mp4'));
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    updateAnimalVideo = await firebaseStorageRef.getDownloadURL() as String;
    setState(() {
      print("Profile Picture uploaded");
    });
  }
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  final FirebaseAuth auth = FirebaseAuth.instance;


  Future updateSellingAnimal() async {
    Firestore.instance
        .collection("SellAnimalList")
        .document(widget.sellingDocumentId)
        .updateData({
      "SellAnimalList": {
        "UserName": userProfileName,
        "UserAddress" : userProfileAddress,
        "UserPhoneNumber" : userProfilePhone,
        "UserId": userId,
        "AnimalBreed": updateAnimalBreed ?? "No data",
        "AnimalCategoryName":widget.animalNameDetail,
        "AnimalImage1": (_image != null) ? updateAnimalImage1: widget.animalImage1Detail,
        "AnimalImage2": (_image2 != null) ? updateAnimalImage1: widget.animalImage1Detail,
        "AnimalImage3": (_image3 != null) ? updateAnimalImage1: widget.animalImage1Detail,
        "AnimalVideo" : (_video != null) ? updateAnimalVideo : widget.animalVideoDetail,
        "AnimalPrice": int.parse(updateAnimalPrice),
        "AnimalPregnancyCount":selectedType ?? widget.animalPregnancyCount,
        "AnimalPregnancyStatus":selectedTypeStatus ?? widget.animalPregnancyStatus,
        "AnimalGender":selectedTypeGender ?? widget.animalGenderDetail,
        "AnimalDescription":updateAnimalDescription ?? "No data",
        "AnimalLocation":widget.animalLocationDetail,
        "CurrentDate":currentDate,
        "CurrentTime" : currentTime,
        "DateTime" : currentDateTime,
        "AnimalCity":widget.city
      },
    }).then((value) {});
//
  }
  var currentDateTime;
  String formattedDate;
  var currentDate;
  var currentTime;
  var now;
  @override
  void initState() {
    super.initState();
    getUserProfile();
    getCurrentUser();
    now  = new DateTime.now();
    currentDate = DateFormat("dd-MM-yyyy").format(now);
    currentTime = DateFormat("H:m:s").format(now);
    print("images ${widget.sellingDocumentId}");
  }

  void dispose() {
    // Clean up the controller when the widget is disposed.

    super.dispose();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightGreen[500],
          title: Text(
            translator.translate('Update Animal Sale Post'),
            style: TextStyle(fontSize: 15, fontFamily: 'Righteous',color: Colors.white),
          ),
          bottom: PreferredSize(
              child: Container(
                color: Colors.black87,
                height: 8.0,
              ),
              preferredSize: Size.fromHeight(8.0)),
        ),
        floatingActionButton: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  setState(() {
                    isLoading = true;
                  });
                  DateTime now = new DateTime.now();
                  formattedDate = DateFormat('dd-MM-yyyy h:mma').format(now);
                  currentDateTime = formattedDate;
                  updateSellingAnimal();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ViewPersonalSellingList()));
                }
              },
              icon: Icon(
                Icons.send,
                color: Colors.white,
              ),
              label: Text(
                translator.translate('Update'),
                style:
                TextStyle(fontFamily: 'Righteous', color: Colors.white),
              ),
              backgroundColor: Colors.lightGreen[600],
            ),
          ],
        ),
        backgroundColor: Colors.white,
        body: ListView(children: [
          Container(
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: Text(
                        translator.translate('Update Images'),
                        style: TextStyle(
                            fontSize: 16, fontFamily: 'Righteous', color: Colors.black),
                      ),
                    ),

                    Container(
                      margin: const EdgeInsets.all(10),
                      height: 100,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                      child:Container(
                          decoration: BoxDecoration(
                          border: Border.all(
                          color: Colors.green
                      ),
                    ),
                            child:
                            InkWell(
                              onTap:() {
                                getImage();
                              },
                              child:

                              ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: (_image != null)
                                    ? Image.file(
                                  _image,
                                  width: 20,
                                  height: 50,

                                )
                                    :  FadeInImage(
                                  image:
                                  NetworkImage(
                                      widget.animalImage1Detail.toString()),
                                  fit: BoxFit.fill,
                                  width: 20,
                                  height: 50,
                                  placeholder: AssetImage(
                                      'assets/images/loading.gif',),

                                ),


                              ),
                            ),
                          ),
                          ),
                          Expanded(
                            child:Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.green
                                ),
                              ),
                            child:
                            InkWell(
                              onTap:() {
                                getImage2();
                              },
                              child:

                              ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: (_image2 != null)
                                    ? Image.file(
                                  _image2,
                                  width: 20,
                                  height: 50,

                                )
                                    :   FadeInImage(
                                  image:
                                  NetworkImage(
                                      widget.animalImage2Detail.toString()),
                                  fit: BoxFit.fill,
                                  width: 20,
                                  height: 50,
                                  placeholder: AssetImage(
                                    'assets/images/loading.gif',),

                                ),


                              ),
                            ),
                          ),
                          ),
                          Expanded(
                            child:Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.green
                                ),
                              ),
                            child:
                            InkWell(
                              onTap:() {
                                getImage3();
                              },
                              child:

                              ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: (_image3 != null)
                                    ? Image.file(
                                  _image3,
                                  width: 20,
                                  height: 50,

                                )
                                    :  FadeInImage(
                                  image:
                                  NetworkImage(
                                      widget.animalImage3Detail.toString()),
                                  fit: BoxFit.fill,
                                  width: 20,
                                  height: 50,
                                  placeholder: AssetImage(
                                    'assets/images/loading.gif',),

                                ),


                              ),
                            ),
                          ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: Text(
                        translator.translate('Update Video'),
                        style: TextStyle(
                            fontSize: 16, fontFamily: 'Righteous', color: Colors.black),
                      ),
                    ),
                    Container(
                      height: 200,

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8.0),
                              child: (_video != null)
                                  ? AspectRatio(
                                  aspectRatio: 23 / 17,
                                  child: FlickVideoPlayer(
                                    flickManager: FlickManager(
                                        videoPlayerController: VideoPlayerController.file(_video)),
                                    flickVideoWithControls: FlickVideoWithControls(
                                      controls: FlickPortraitControls(),
                                    ),
                                  )
                              )
                                  :  AspectRatio(
                                  aspectRatio: 23 / 17,
                                  child: FlickVideoPlayer(
                                    flickManager: FlickManager(
                                        videoPlayerController: VideoPlayerController.network(widget.animalVideoDetail)),
                                    flickVideoWithControls: FlickVideoWithControls(
                                      controls: FlickPortraitControls(),
                                    ),
                                  )
                              )

                            ),),
                          Padding(
                            padding: EdgeInsets.only(top: 30.0),
                            child: IconButton(
                              icon: Icon(
                                FontAwesomeIcons.camera,
                                size: 30.0,
                              ),
                              onPressed: () {
                                getVideo();
//                                    uploadPic(context);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 20,
                    ),
                    Column(
                        children:[
                          Container(
                            margin: EdgeInsets.all(10),
                            child: TextFormField(
                              enabled: false,
                              initialValue:widget.animalLocationDetail ,
                              decoration: InputDecoration(
                                labelText: translator.translate("Location"),
                                prefixIcon: Icon(
                                  Icons.location_pin,
                                  color: Colors.lightGreen[400],
                                ),

                                fillColor: Colors.white,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(25.0),
                                  borderSide: new BorderSide(),
                                ),
                                //fillColor: Colors.green
                              ),

                            ),
                          ),
                        ]
                    ),

                    SizedBox(height: 10.0),
                    Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.all(10),
                      child: TextFormField(
                        enabled: false,
                        initialValue: "${widget.animalNameDetail}",
                        decoration: InputDecoration(
                          labelText: translator.translate("Animal Category"),
                          prefixIcon: Icon(
                            Icons.category,
                            color: Colors.lightGreen[400],
                          ),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),

                      ),
                    ),

                    SizedBox(height: 10.0),
                    widget.animalPregnancyCount == "No data"
                    ?Container():
                    Container(
                      // alignment: Alignment.topCenter,
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: DropdownButton(

                          items: pregnanciesCount
                              .map((value) => DropdownMenuItem(
                            child: Text(
                              value,
                              style: TextStyle(color: Colors.black),
                            ),
                            value: value,
                          ))
                              .toList(),
                          onChanged: (selectedPregnancy) {
                            setState(() {
                              selectedType = selectedPregnancy;
                            });
                          },
                          value: selectedType,
                          isExpanded: false,
                          hint: Text(
                            ('${translator.translate(widget.animalPregnancyCount)}'),
                            style: TextStyle(color: Colors.lightGreen[500]),
                          ),
                        )

                    ),
                    Container(
                      // alignment: Alignment.topCenter,
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: DropdownButton(
                          items: updateAnimalGender
                              .map((value) => DropdownMenuItem(
                            child: Text(
                              value,
                              style: TextStyle(color: Colors.black),
                            ),
                            value: value,
                          ))
                              .toList(),
                          onChanged: (selectedGender) {
                            setState(() {
                              selectedTypeGender = selectedGender;
                            });
                          },
                          value: selectedTypeGender,
                          isExpanded: false,
                          hint: Text(
                            ('${translator.translate(widget.animalGenderDetail)}'),
                            style: TextStyle(color: Colors.lightGreen[500]),
                          ),
                        )

                    ),


                    SizedBox(
                      height: 10,
                    ),
                    widget.animalPregnancyStatus == "No data"
                        ?Container():
                    Container(
                      // alignment: Alignment.topCenter,
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: DropdownButton(
                          items: pregnanciesStatus
                              .map((value) => DropdownMenuItem(
                            child: Text(
                              value,
                              style: TextStyle(color: Colors.black),
                            ),
                            value: value,
                          ))
                              .toList(),
                          onChanged: (selectedPregnancyStatus) {
                            setState(() {
                              selectedTypeStatus = selectedPregnancyStatus;
                            });
                          },
                          value: selectedTypeStatus,
                          isExpanded: false,
                          hint: Text(
                            ('${translator.translate(widget.animalPregnancyStatus)}'),
                            style: TextStyle(color: Colors.lightGreen[500]),
                          ),
                        )

                    ),

                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.all(10),
                      child: TextFormField(
                        initialValue: "${widget.animalBreedDetail}",
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                          labelText: translator.translate("Breed"),
                          hintText: translator.translate("Breed"),
                          prefixIcon: Icon(
                            Icons.text_format_outlined,
                            color: Colors.lightGreen[400],
                          ),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),
                        onSaved: (value) {
                          updateAnimalBreed = value;
                        },
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.all(10),
                      child: TextFormField(
                        initialValue: "${widget.animalPriceDetail}",
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: translator.translate("Price"),
                          hintText: "123",
                          prefixIcon: Icon(
                            Icons.attach_money,
                            color: Colors.lightGreen[400],
                          ),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return translator.translate('Enter your animal price');
                          }
                          return null;
                        },
                        onSaved: (value) {
                          updateAnimalPrice = value;
                        },
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.all(10),
                      child: TextFormField(
                        initialValue: "${widget.animalDescriptionDetail}",
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                          labelText: translator.translate("Description"),
                          hintText: translator.translate("Description"),
                          prefixIcon: Icon(
                            Icons.note,
                            color: Colors.lightGreen[400],
                          ),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),
                        onSaved: (value) {
                         updateAnimalDescription = value;
                        },
                      ),
                    ),
                    SizedBox(height: 10.0),
                  ],
                ),
              ),
            ),
          ),
        ]));

  }
}
