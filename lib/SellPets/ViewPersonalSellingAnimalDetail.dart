import 'package:apna_beepar/SellPets/AddAnimal.dart';
import 'package:apna_beepar/SellPets/ViewPersonalSellingList.dart';
import 'package:apna_beepar/UserHomePage/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:toast/toast.dart';
import 'package:apna_beepar/SellPets/UpdateSellingAnimalDetail.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:flick_video_player/flick_video_player.dart';
import 'package:video_player/video_player.dart';

class ViewPersonalSellingAnimalDetail extends StatefulWidget {
  var animalPriceDetail;
  final String animalNameDetail;
  final String animalPregnancyStatus;
  final String animalPregnancyCount;
  var animalImage1Detail;
  var animalImage2Detail;
  var animalImage3Detail;
  var animalAllImages=[];
  final String animalVideoDetail;
  final String animalDescriptionDetail;
  final String animalLocationDetail;
  final String animalGenderDetail;
  final String animalBreedDetail;
  final String sellingDocumentId;
  var city;
  @override
  _ViewPersonalSellingAnimalDetailState createState() =>
      new _ViewPersonalSellingAnimalDetailState();
  ViewPersonalSellingAnimalDetail({Key key,
    @required this.animalDescriptionDetail,
    @required this.animalPregnancyCount,
    @required this.animalPregnancyStatus,
    @required this.animalPriceDetail,
    @required this.animalNameDetail,
    @required this.animalImage1Detail,
    @required this.animalImage2Detail,
    @required this.animalImage3Detail,
    @required this.animalLocationDetail,
    @required this.animalGenderDetail,
    @required this.animalBreedDetail,
    @required this.animalAllImages,
    @required this.sellingDocumentId,
    @required this.animalVideoDetail,
    @required this.city,

  }) : super(key: key);
}

class _ViewPersonalSellingAnimalDetailState extends State<ViewPersonalSellingAnimalDetail> {
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }
 int _current = 0;
List images = [];
var deleteSellingItem;
  Future deleteSellingData(deleteSellingItem) async {
    Firestore.instance
        .collection("SellAnimalList")
        .document(deleteSellingItem)
        .delete()
        .then((_) {});
  }
  FlickManager flickManager;
  @override
  void initState() {
    super.initState();
    images = widget.animalAllImages;
   print("images ${widget.sellingDocumentId}");
    flickManager =FlickManager(
        videoPlayerController: VideoPlayerController.network(widget.animalVideoDetail));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.lightGreen[500],
              title: Text(
                translator.translate('My Selling List'),
                style: TextStyle(
                    fontSize: 18, fontFamily: 'Righteous', color: Colors.white),
              ),
              bottom: PreferredSize(
                  child: Container(
                    color: Colors.black87,
                    height: 8.0,
                  ),
                  preferredSize: Size.fromHeight(8.0)),
            ),
            backgroundColor: Colors.white,
            body: Center(
              child: ListView(
                  children:<Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Column(
                        children: [
                        CarouselSlider(
                          options: CarouselOptions(
                        aspectRatio: 20 / 14,
                        scrollDirection: Axis.horizontal,
                        enlargeCenterPage: true,
                        reverse: false,
                        viewportFraction: 1.0,
                              onPageChanged: (index, reason) {
                                setState(() {
                                  _current = index;
                                });
                              }
                    ),
                        items : map<Widget>(
                          widget.animalAllImages,
                              (index, i) {
                            return Container(
                              margin: EdgeInsets.all(7.0),
                              child: ClipRRect(
                                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                child: Stack(children: <Widget>[
                                  FadeInImage(
                                    image: NetworkImage(
                                        i.toString()),
                                    fit: BoxFit.cover,
                                    width: 1000,

                                    placeholder: AssetImage(
                                        'assets/images/loading.gif'),
                                  ),
                                  InkWell(

                                    child:FadeInImage(
                                      image: NetworkImage(
                                          i.toString()),
                                      fit: BoxFit.cover,
                                      width: 1000,

                                      placeholder: AssetImage(
                                          'assets/images/loading.gif'),
                                    ),),

                                ]),
                              ),
                            );
                          },
                        ).toList()
                    ),


            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: map<Widget>(
                widget.animalAllImages,
                    (index, url) {
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _current == index
                            ? Color.fromRGBO(0, 0, 0, 0.9)
                            : Color.fromRGBO(0, 0, 0, 0.4)),
                  );
                },
              ),
            ),
                        ]
                    ),
                    Container(
                        margin: const EdgeInsets.all(10),
                        child:Text(
                          "Animal Video",
                          style: TextStyle(fontFamily: 'Righteous',fontSize: 17),
                        )
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child:ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: (widget.animalVideoDetail != null)
                              ? AspectRatio(
                              aspectRatio: 20 / 10,
                              child: FlickVideoPlayer(
                                flickManager: flickManager,
                                flickVideoWithControls: FlickVideoWithControls(
                                  controls: FlickPortraitControls(),
                                ),
                              )
                          )
                              : Center(
                              child:Text(
                                "No Video Available",
                                style: TextStyle(fontFamily: 'Righteous',fontSize: 16),
                              )
                          )

                      ),
                    ),

                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      margin: const EdgeInsets.all(5),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.lightGreen[100],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(

                              title: Text(translator.translate("Location: ") + widget.animalLocationDetail.toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Righteous',
                                      fontSize: 16)),
                              leading: Icon(Icons.location_on_sharp),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(5),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.lightGreen[100],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(

                              title: Text(translator.translate("Animal Category: ") + widget.animalNameDetail.toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Righteous',
                                      fontSize: 15)),

                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(5),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.lightGreen[100],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(

                              title: Text(translator.translate("Price: ") + widget.animalPriceDetail.toString()+translator.translate("Rs."),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Righteous',
                                      fontSize: 15)),

                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(5),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.lightGreen[100],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(

                              title: Text(translator.translate("Breed: ") + widget.animalBreedDetail.toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Righteous',
                                      fontSize: 15)),

                            ),
                          ],
                        ),
                      ),
                    ),
                    widget.animalPregnancyStatus.isEmpty
                    ?Container():
                    Container(
                      margin: const EdgeInsets.all(5),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.lightGreen[100],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(

                              title: Text(translator.translate("Pregnancy Status: ") +translator.translate(widget.animalPregnancyStatus.toString()),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Righteous',
                                      fontSize: 16)),

                            ),
                          ],
                        ),
                      ),
                    ),
                    widget.animalPregnancyCount.isEmpty
                    ?Container():
                    Container(
                      margin: const EdgeInsets.all(5),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.lightGreen[100],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(

                              title: Text(translator.translate("Pregnancy Count: ") + translator.translate(widget.animalPregnancyCount.toString()),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Righteous',
                                      fontSize: 16)),

                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 200,
                      margin: const EdgeInsets.all(5),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.lightGreen[100],
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(

                              title: Text(translator.translate("Description: ") + widget.animalDescriptionDetail.toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'Righteous',
                                      fontSize: 16)),
                              leading: Icon(Icons.note),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                        height: 50.0,
                        width: 400,
                        padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 0.0),
                        child: RaisedButton.icon(
                          icon: Icon(
                            Icons.update,
                            color: Colors.white,
                          ),
                          shape: RoundedRectangleBorder(

                              side: BorderSide()),
                          color: Colors.lightGreen[600],
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => UpdateAnimalDetails(
                                      city: widget.city,
                                      sellingDocumentId: widget.sellingDocumentId,
                                      animalLocationDetail: widget.animalLocationDetail,
                                      animalBreedDetail: widget.animalBreedDetail,
                                      animalDescriptionDetail: widget.animalDescriptionDetail,
                                      animalGenderDetail: widget.animalGenderDetail,
                                      animalPregnancyCount: widget.animalPregnancyCount,
                                      animalPregnancyStatus: widget.animalPregnancyStatus,
                                      animalPriceDetail: widget.animalPriceDetail,
                                      animalNameDetail: widget.animalNameDetail,
                                      animalImage3Detail: widget.animalImage3Detail,
                                      animalImage2Detail: widget.animalImage2Detail,
                                      animalImage1Detail: widget.animalImage1Detail,
                                      animalVideoDetail: widget.animalVideoDetail,

                                    )));
                          },

                          label: Text(
                            translator.translate('Update'),
                            style: TextStyle(fontFamily: 'Righteous',color: Colors.white,fontSize: 17),
                          ),
                        )),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                        height: 50.0,
                        width: 400,
                        padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 0.0),
                        child: RaisedButton.icon(
                          icon: Icon(
                            CupertinoIcons.delete,
                            color: Colors.white,
                          ),
                          shape: RoundedRectangleBorder(

                              side: BorderSide()),
                          color: Colors.lightGreen[600],
                          onPressed: () {
                            deleteSellingItem = widget.sellingDocumentId;
                            deleteSellingData(deleteSellingItem);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ViewPersonalSellingList()));
                            Toast.show(translator.translate('Your data has deleted successfully'), context,duration: 2);
                          },

                          label: Text(
                            translator.translate('Delete'),
                            style: TextStyle(fontFamily: 'Righteous',color: Colors.white,fontSize: 17),
                          ),
                        )),
                    SizedBox(
                      height: 20,
                    )
                  ]
                      ),
            ));
  }
}
