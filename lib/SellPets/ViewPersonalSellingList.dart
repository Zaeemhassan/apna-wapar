import 'package:apna_beepar/SellPets/AddAnimal.dart';
import 'package:apna_beepar/SellPets/ViewPersonalSellingAnimalDetail.dart';
import 'package:apna_beepar/UserHomePage/HomePage.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geolocator/geolocator.dart';
import 'package:localize_and_translate/localize_and_translate.dart';

class ViewPersonalSellingList extends StatefulWidget {
  @override
  _ViewPersonalSellingListState createState() =>
      new _ViewPersonalSellingListState();
}

class _ViewPersonalSellingListState extends State<ViewPersonalSellingList> {
  bool isLoading = true;
  var id;
  List itemsData = [];
  var animalName = [];
  var animalImage1 = [];
  var animalImage2 = [];
  var animalImage3 = [];
  var animalPrice = [];
  var animalSellerLocation = [];
  var animalCategory = [];
  var pregnancyCount = [];
  var animalGender = [];
  var pregnancyStatus = [];
  var animalBreed = [];
  var animalDescription = [];
  var animalUserId;
  var animalVideo = [];
  List document = [];
  var city = [];
  var allImages=[];
  var deleteCategoryData;
  bool dataCheck = false;
  String _currentAddress;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  TextEditingController searchController = new TextEditingController();
  String filter;
  _getCurrentLocation() {
    setState(() {
      isLoading = true;
    });
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
      setState(() {
        isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress = "${place.locality}, ${place.country}";
      });
      print('address $_currentAddress');
    } catch (e) {
      print(e);
    }
  }
  final FirebaseAuth auth = FirebaseAuth.instance;
  Future getAnimalSellingData() async {
    setState(() {
      isLoading = true;
    });
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final userId = user.uid;
    Firestore.instance
        .collection("SellAnimalList")
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        var sellingData = (result.data["SellAnimalList"]);
        animalUserId=(sellingData['UserId']);
        if(animalUserId == userId) {
          animalName.add(sellingData['AnimalCategoryName']);
          animalImage1.add(sellingData['AnimalImage1']);
          animalImage2.add(sellingData['AnimalImage2']);
          animalImage3.add(sellingData['AnimalImage3']);
          animalBreed.add(sellingData['AnimalBreed']);
          animalSellerLocation.add(sellingData['AnimalLocation']);
          pregnancyCount.add(sellingData['AnimalPregnancyCount']);
          pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
          animalGender.add(sellingData['AnimalGender']);
          animalDescription.add(sellingData['AnimalDescription']);
          animalPrice.add(sellingData['AnimalPrice']);
          animalVideo.add(sellingData['AnimalVideo']);
          city.add(sellingData["AnimalCity"]);
        }
        else{
          setState(() {
            isLoading=true;
          });
        }
        print("selli $sellingData");
        if (sellingData.isNotEmpty) {
          setState(() {
            dataCheck = true;
          });
        }

      });

      setState(() {
        isLoading = false;
      });
    });
  }

  Future deleteCategory(deleteCategoryData) async {
    Firestore.instance
        .collection("Animal Category")
        .document(deleteCategoryData)
        .delete()
        .then((_) {});
  }
  BannerAd _bannerAd;
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo();
  BannerAd createBannerAd() {
    return new BannerAd(
        adUnitId: "ca-app-pub-9603407988877715/3498665226",
        size: AdSize.banner,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("Banner event : $event");
        });
  }
  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
        adUnitId: "ca-app-pub-9603407988877715/8189731030",
        //Change Interstitial AdUnitId with Admob ID
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("IntersttialAd $event");
        });
  }
  @override
  void initState() {
    FirebaseAdMob.instance
        .initialize(appId: "ca-app-pub-9603407988877715~1895785423");
    _bannerAd = createBannerAd()
      ..load()
      ..show();
   getAnimalSellingData();
    _getCurrentLocation();
    super.initState();
   searchController.addListener(() {
     setState(() {
       filter = searchController.text;
     });
   });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    searchController.dispose();
    _bannerAd?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.lightGreen[500],
              title: Text(
                translator.translate('My Selling List'),
                style: TextStyle(
                    fontSize: 18, fontFamily: 'Righteous', color: Colors.white),
              ),
              bottom: PreferredSize(
                  child: Container(
                    color: Colors.black87,
                    height: 8.0,
                  ),
                  preferredSize: Size.fromHeight(8.0)),

            ),
            backgroundColor: Colors.white,
            body: isLoading
                ? SpinKitDoubleBounce(
                    color: Colors.greenAccent[700],
                  )
                : Center(
                    child: ListView(
                        children: dataCheck
                            ? <Widget>[
                                SizedBox(
                                  height: 10,
                                ),
                          Container(
                            margin: const EdgeInsets.fromLTRB(100, 10, 10, 10),
                            child: Card(
                              child: TextField(
                                controller: searchController,
                                decoration: InputDecoration(
                                  hintText: "Search",
                                  prefixIcon: Icon(
                                    Icons.search,
                                    color: Colors.green,
                                  ),
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius: new BorderRadius.circular(0.0),
                                    borderSide: new BorderSide(),
                                  ),
                                  //fillColor: Colors.green
                                ),
                                style: TextStyle(color: Colors.black, fontSize: 16.0),
                              ),
                            ),
                          ),
                                Container(
                                  margin: const EdgeInsets.all(10),
                                  child: GridView.builder(
                                      shrinkWrap: true,
                                      physics: BouncingScrollPhysics(),
                                      itemCount: animalName?.length ?? 0,
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 1),
                                      itemBuilder: (context, index) {
                                        return filter == null || filter == ""
                                            ?
                                        InkWell(

                                          splashColor: Colors.green,
                                          onTap: () {

                                            allImages.clear();
                                            allImages.add(animalImage1[index]);
                                            allImages.add(animalImage2[index]);
                                            allImages.add(animalImage3[index]);
                                            createInterstitialAd()
                                              ..load()
                                              ..show();
                                            Navigator.push(
                                              context,
                                              new MaterialPageRoute(
                                                  builder: (context) =>
                                                      ViewPersonalSellingAnimalDetail(
                                                        city: city[index],
                                                        animalNameDetail: animalName[index],
                                                        animalLocationDetail: animalSellerLocation[index],
                                                        animalDescriptionDetail: animalDescription[index],
                                                        animalGenderDetail: animalGender[index],
                                                        animalAllImages: allImages,
                                                        animalPregnancyCount: pregnancyCount[index],
                                                        animalPregnancyStatus: pregnancyStatus[index],
                                                        animalPriceDetail: animalPrice[index],
                                                        animalBreedDetail: animalBreed[index],
                                                        sellingDocumentId: document[index],
                                                        animalImage1Detail: animalImage1[index],
                                                        animalImage2Detail: animalImage2[index],
                                                        animalImage3Detail: animalImage3[index],
                                                        animalVideoDetail: animalVideo[index],
                                                      )),
                                            );
                                          },
                                          child: Card(
                                            elevation: 8,
                                            semanticContainer: true,
                                            color: Colors.white,
                                            child: Container(
//                            width: 100.0,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                BorderRadius.all(
                                                    Radius.elliptical(
                                                        60.0, 60.0)),
//                                  color: Colors.indigo[200],
                                              ),
                                              child: Column(

                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                      BorderRadius
                                                          .circular(15.0),
                                                      child: Image.network(
                                                        animalImage1[index]
                                                            .toString(),
                                                        height: 230.0,
                                                        width: 400.0,
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                    Container(
                                                        margin: const EdgeInsets
                                                            .fromLTRB(
                                                            10, 10, 10, 0),
                                                        child:
                                                        Row(
                                                          children: [
                                                            Expanded(child:
                                                            Text(translator
                                                                .translate(
                                                                "Animal Category: "),
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                  "Righteous",
                                                                  fontSize: 17),
                                                            ),
                                                            ),
                                                            Expanded(
                                                              flex: 1,
                                                              child:
                                                              Text(
                                                                animalName[index]
                                                                    .toString(),
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                    "Righteous",
                                                                    fontSize: 17),
                                                              ),
                                                            )
                                                          ],
                                                        )
                                                    ),

                                                    Container(
                                                        margin: const EdgeInsets
                                                            .fromLTRB(
                                                            10, 10, 10, 10),
                                                        child:
                                                        Row(
                                                          children: [
                                                            Expanded(child:
                                                            Text(translator
                                                                .translate(
                                                                "Price: "),
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                  "Righteous",
                                                                  fontSize: 17),
                                                            ),
                                                            ),
                                                            Expanded(
                                                              flex: 3,
                                                              child:
                                                              Text(
                                                                animalPrice[index]
                                                                    .toString() +
                                                                    translator
                                                                        .translate(
                                                                        "Rs."),
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                    "Righteous",
                                                                    fontSize: 17),
                                                              ),
                                                            )
                                                          ],
                                                        )
                                                    ),


                                                  ]),
                                            ),
                                          ),
                                        ):
                                        '${animalName[index]}'
                                            .toLowerCase()
                                            .contains(filter.toLowerCase())
                                            ?
                                        InkWell(
                                          splashColor: Colors.green,
                                          onTap: () {
                                            allImages.clear();
                                            allImages.add(animalImage1[index]);
                                            allImages.add(animalImage2[index]);
                                            allImages.add(animalImage3[index]);
                                            createInterstitialAd()
                                              ..load()
                                              ..show();
                                            Navigator.push(
                                              context,
                                              new MaterialPageRoute(
                                                  builder: (context) =>
                                                      ViewPersonalSellingAnimalDetail(
                                                        city: city[index],
                                                        animalNameDetail: animalName[index],
                                                        animalLocationDetail: animalSellerLocation[index],
                                                        animalDescriptionDetail: animalDescription[index],
                                                        animalGenderDetail: animalGender[index],
                                                        animalAllImages: allImages,
                                                        animalPregnancyCount: pregnancyCount[index],
                                                        animalPregnancyStatus: pregnancyStatus[index],
                                                        animalPriceDetail: animalPrice[index],
                                                        animalBreedDetail: animalBreed[index],
                                                        sellingDocumentId: document[index],
                                                        animalImage1Detail: animalImage1[index],
                                                        animalImage2Detail: animalImage2[index],
                                                        animalImage3Detail: animalImage3[index],
                                                        animalVideoDetail: animalVideo[index],
                                                      )),
                                            );
                                          },
                                          child: Card(
                                            elevation: 8,
                                            semanticContainer: true,
                                            color: Colors.white,
                                            child: Container(
//                            width: 100.0,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                BorderRadius.all(
                                                    Radius.elliptical(
                                                        60.0, 60.0)),
//                                  color: Colors.indigo[200],
                                              ),
                                              child: Column(

                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                      BorderRadius
                                                          .circular(15.0),
                                                      child: Image.network(
                                                        animalImage1[index]
                                                            .toString(),
                                                        height: 230.0,
                                                        width: 400.0,
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                    Container(
                                                        margin: const EdgeInsets
                                                            .fromLTRB(
                                                            10, 10, 10, 0),
                                                        child:
                                                        Row(
                                                          children: [
                                                            Expanded(child:
                                                            Text(translator
                                                                .translate(
                                                                "Animal Category: "),
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                  "Righteous",
                                                                  fontSize: 17),
                                                            ),
                                                            ),
                                                            Expanded(
                                                              flex: 1,
                                                              child:
                                                              Text(
                                                                animalName[index]
                                                                    .toString(),
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                    "Righteous",
                                                                    fontSize: 17),
                                                              ),
                                                            )
                                                          ],
                                                        )
                                                    ),

                                                    Container(
                                                        margin: const EdgeInsets
                                                            .fromLTRB(
                                                            10, 10, 10, 10),
                                                        child:
                                                        Row(
                                                          children: [
                                                            Expanded(child:
                                                            Text(translator
                                                                .translate(
                                                                "Price: "),
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                  "Righteous",
                                                                  fontSize: 17),
                                                            ),
                                                            ),
                                                            Expanded(
                                                              flex: 3,
                                                              child:
                                                              Text(
                                                                animalPrice[index]
                                                                    .toString() +
                                                                    translator
                                                                        .translate(
                                                                        "Rs."),
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                    "Righteous",
                                                                    fontSize: 17),
                                                              ),
                                                            )
                                                          ],
                                                        )
                                                    ),


                                                  ]),
                                            ),
                                          ),
                                        ) :new Container();
                                      }
                                              ),
                                ),
                                SizedBox(
                                  height: 20,
                                )
                              ]
                            : <Widget>[
                                Container(
                                    padding: EdgeInsets.fromLTRB(
                                        50.0, 100.0, 50.0, 50.0),
                                    child: Center(
                                      child: Text(
                                        translator.translate("No data available"),
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 25.0,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Righteous',
                                          //decoration: TextDecoration.none
                                        ),
                                      ),
                                    ))
                              ]),
                 ));
  }
}
