
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:apna_beepar/UserHomePage/HomePage.dart';


class Setting extends StatefulWidget {
  @override
  _SettingState createState() => new _SettingState();
}

class _SettingState extends State<Setting> {
//
  BannerAd _bannerAd;
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo();
  BannerAd createBannerAd() {
    return new BannerAd(
        adUnitId: "ca-app-pub-9603407988877715/3498665226",
        size: AdSize.banner,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("Banner event : $event");
        });
  }
  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
        adUnitId: "ca-app-pub-9603407988877715/8189731030",
        //Change Interstitial AdUnitId with Admob ID
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print("IntersttialAd $event");
        });
  }
  @override
  void initState() {
    super.initState();
    FirebaseAdMob.instance
        .initialize(appId: "ca-app-pub-9603407988877715~1895785423");
    _bannerAd = createBannerAd()
      ..load()
      ..show();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bannerAd?.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[500],
        title: Text(
          translator.translate('Setting'),
          style: TextStyle(
              fontSize: 18, fontFamily: 'Righteous', color: Colors.white),
        ),
        bottom: PreferredSize(
            child: Container(
              color: Colors.black87,
              height: 8.0,
            ),
            preferredSize: Size.fromHeight(8.0)),

      ),
      backgroundColor: Colors.white,
      body:  ListView(
        children: [
          SizedBox(
            height: 30,
          ),
          Container(
            margin: const EdgeInsets.all(6),
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  color: Colors.white,
                  child: Center(

                    child:
                      ListTile(
                        title: Text(translator.translate("Select Language"),
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Righteous',
                                fontSize: 17)),
                      ),
                  ),
                )
            ),
         SizedBox(
           height: 20,
         ),
          Container(
            margin: const EdgeInsets.all(6),
            child:
            InkWell(
                onTap: (){
                  createInterstitialAd()
                    ..load()
                    ..show();
                  translator.setNewLanguage(
                    context,
                    newLanguage: translator.currentLanguage == 'en' ? 'en' : 'en',
                    remember: true,
                    restart: true,
                  );
                },
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        leading:Image.asset(

                            "assets/images/united-states.png"
                        ),
                        title: Text(translator.translate("English"),
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Righteous',
                                fontSize: 17)),
                      ),

                    ],
                  ),
                )
            ),
          ),
          Container(
            margin: const EdgeInsets.all(6),
            child:
                InkWell(
                  onTap: (){
                    createInterstitialAd()
                      ..load()
                      ..show();
                    translator.isDirectionRTL(context);
                    translator.setNewLanguage(
                      context,
                      newLanguage: translator.currentLanguage == 'ur' ? 'ur' : 'ur',
                      remember: true,
                      restart: true,
                    );
                  },
            child: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading:Image.asset(

                  "assets/images/pakistan.png"
              ),
                    title: Text(translator.translate("Urdu"),
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'Righteous',
                            fontSize: 17)),
                  ),

                ],
              ),
    )
            ),
          ),

          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
