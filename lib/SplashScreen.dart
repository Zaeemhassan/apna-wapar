import 'dart:async';
import 'package:apna_beepar/UserHomePage/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:apna_beepar/SplashScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:apna_beepar/UserLogin/LoginWithPhoneNumber.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    var auth = FirebaseAuth.instance;
    auth.onAuthStateChanged.listen((user) {
      if (user != null) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => UserHomePage()));
      } else {
        setState(() {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => LoginPhone()));
        });

      }
    });
  }

  @override
  void initState() {
    super.initState();
    startTime();

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: Container(

                        child: Image.asset(
                          'assets/images/logo-app.png',
                          width: 300,
                          height: 300,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                padding: EdgeInsets.fromLTRB(0, 80, 0, 0),
                child: new CircularProgressIndicator(
                  backgroundColor: Colors.black,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}