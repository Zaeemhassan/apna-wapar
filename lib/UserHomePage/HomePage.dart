import 'dart:io';
import 'package:apna_beepar/HelpPage/HelpScreen.dart';
import 'package:apna_beepar/BuyPets/ViewBuyAnimalaList.dart';
import 'package:apna_beepar/Profile/UserProfile.dart';
import 'package:apna_beepar/SellPets/SellAnimalsCategory.dart';
import 'package:apna_beepar/SellPets/ViewPersonalSellingList.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:apna_beepar/Settings/LanguagePage.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:android_intent/android_intent.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../SplashScreen.dart';
import 'package:permission_handler/permission_handler.dart';

class UserHomePage extends StatefulWidget {
  @override
  _UserHomePageState createState() => _UserHomePageState();
}

class _UserHomePageState extends State<UserHomePage> {
  bool isLoading = false;
  var id;
  var compareAnimalName;
  var documnet = [];
  List itemsData = [];
  var animalName = [];
  var animalImage = [];
  List document = [];
  var deleteCategoryData;
  bool dataCheck = false;
  var animalId;
  List allAnimalsData = [];
  var animalBuyName = [];
  var animalImage1 = [];
  var animalImage2 = [];
  var animalImage3 = [];
  var animalPrice = [];
  var animalSellerLocation = [];
  var animalCategory = [];
  var pregnancyCount = [];
  var animalGender = [];
  var pregnancyStatus = [];
  var animalBreed = [];
  var animalDescription = [];
  var animalUserId;
  var userSelectedAnimalName = [];
  var userSelectedAnimalImage1 = [];
  var userSelectedAnimalImage2 = [];
  var userSelectedAnimalImage3 = [];
  var userSelectedAnimalBreed = [];
  var userSelectedAnimalPrice = [];
  var userSelectedAnimalDescription = [];
  var userSelectedAnimalPregnancyCount = [];
  var userSelectedAnimalPregnancyStatus = [];
  var userSelectedAnimalGender = [];
  var userSelectedAnimalLocation = [];
  List listDocument = [];
  var sellingData;
  TextEditingController searchController = new TextEditingController();
  String filter;
  var allImages = [];
  String _currentAddress;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  InterstitialAd myInterstitial;
  var helpingVideo;
  TextStyle defaultStyle =
  TextStyle(color: Colors.black, fontSize: 16.0, fontFamily: 'Righteous');


  Future getHelpVideo() async {
    Firestore.instance
        .collection("HelpVideo")
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        var helpVideo = (result.data);
        print("result ${result.data}");
        helpingVideo = (helpVideo['VideoUrl']);

      });

    });
//
  }

  Future getAnimalSellingData() async {

    Firestore.instance
        .collection("SellAnimalList")
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);
        sellingData = (result.data["SellAnimalList"]);
        allAnimalsData.add(result.data['SellAnimalList']);
        animalUserId = (sellingData['UserId']);

        animalBuyName.add(sellingData['AnimalCategoryName']);
        animalImage1.add(sellingData['AnimalImage1']);
        animalImage2.add(sellingData['AnimalImage2']);
        animalImage3.add(sellingData['AnimalImage3']);
        animalBreed.add(sellingData['AnimalBreed']);
        animalSellerLocation.add(sellingData['AnimalLocation']);
        pregnancyCount.add(sellingData['AnimalPregnancyCount']);
        pregnancyStatus.add(sellingData['AnimalPregnancyStatus']);
        animalGender.add(sellingData['AnimalGender']);
        animalDescription.add(sellingData['AnimalDescription']);
        animalPrice.add(sellingData['AnimalPrice']);
      });

    });
  }

  _getCurrentLocation() {

    geolocator
        .getCurrentPosition()
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();

    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress = "${place.locality}, ${place.country}";
      });
      print('address $_currentAddress');
    } catch (e) {
      print(e);
    }
  }
  void openLocationSetting() async {
    final AndroidIntent intent = new AndroidIntent(
      action: 'android.settings.LOCATION_SOURCE_SETTINGS',
    );
    await intent.launch();
  }

  Future getAnimalCategory() async {
    setState(() {
      isLoading = true;
    });
    var connectivityResult =
    await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        (connectivityResult == ConnectivityResult.wifi)) {
      if (await Permission.locationWhenInUse.serviceStatus.isEnabled) {
        setState(() {
          isLoading = true;
        });
        Firestore.instance
            .collection("Animal Category")
            .getDocuments()
            .then((querySnapshot) {
          querySnapshot.documents.forEach((result) {
            id = result.documentID;
            document.add(id);
            var menu = (result.data["Animal Category"]);

//        print("data $category");
            animalName.add(menu['AnimalName']);
            animalImage.add(menu['AnimalImage']);
            if (menu.isNotEmpty) {
              setState(() {
                dataCheck = true;
              });
            }
          });

          setState(() {
            isLoading = false;
          });
        });
      }
      else{
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content:RichText(
                  text: TextSpan(
                    style: defaultStyle,
                    children: <TextSpan>[
                      TextSpan(
                          text:
                          translator.translate('Location not enable !  '),
                          style: TextStyle(
                              fontWeight:
                              FontWeight
                                  .bold,
                              color: Colors.red,
                              fontSize:
                              16)),
                      TextSpan(
                          text:translator.translate("Please enabled location"),
                          style: TextStyle(

                              color: Colors.black,
                              fontSize:
                              16)
                      ),
                    ],
                  ),
                ),
                backgroundColor: Colors.white,

                actions: [
                  FlatButton(
                    child: Text(
                        translator.translate('Ok')
                    ),
                    onPressed: (){
                      Navigator.of(context).pop();
                      openLocationSetting();


                    },
                  ),
                ],
              );
            }
        );
        setState(() {
          isLoading = false;
        });
      }

    }
    else{
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content:RichText(
                text: TextSpan(
                  style: defaultStyle,
                  children: <TextSpan>[
                    TextSpan(
                        text:
                        translator.translate('No internet connection !  '),
                        style: TextStyle(
                            fontWeight:
                            FontWeight
                                .bold,
                            color: Colors.red,
                            fontSize:
                            16)),
                    TextSpan(
                        text:translator.translate("Please connect your mobile to the internet and restart your app"),
                        style: TextStyle(

                            color: Colors.black,
                            fontSize:
                            16)
                    ),
                  ],
                ),
              ),
              backgroundColor: Colors.white,

              actions: [
                FlatButton(
                  child: Text(
                      translator.translate('Ok')
                  ),
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          }
      );
      setState(() {
        isLoading = false;
      });
    }
  }
  final FirebaseAuth auth = FirebaseAuth.instance;
  void _signOut() async {
    await auth.signOut();
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    await Future.delayed(Duration(seconds: 1));

    Navigator.of(context).pushAndRemoveUntil(
      // the new route
      MaterialPageRoute(
        builder: (BuildContext context) => SplashScreen(),
      ),

          (Route route) => false,
    );
  }
  static final MobileAdTargetingInfo targetInfo = new MobileAdTargetingInfo(
    testDevices: <String>[],
    keywords: <String>['wallpapers', 'walls', 'amoled'],
    childDirected: true,
  );
  BannerAd myBanner;
  BannerAd _bannerAd;
  BannerAd createBannerAd() {
    return new BannerAd(
        adUnitId: "ca-app-pub-9179231796573696/7855388181",
        size: AdSize.banner,
        targetingInfo: targetInfo,
        listener: (MobileAdEvent event) {
          print("Banner event : $event");
        });
  }
  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
        adUnitId: "ca-app-pub-9603407988877715/8189731030",
        //Change Interstitial AdUnitId with Admob ID
        targetingInfo: targetInfo,
        listener: (MobileAdEvent event) {
          print("IntersttialAd $event");
        });
  }
  Future<bool> _onWillPop() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding:
            EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0, bottom: 0.0),
            children: <Widget>[
              Container(
                color: Colors.red,
                margin: EdgeInsets.all(0.0),
                padding: EdgeInsets.only(bottom: 10.0, top: 10.0),
                height: 100.0,
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.exit_to_app,
                        size: 30.0,
                        color: Colors.white,
                      ),
                      margin: EdgeInsets.only(bottom: 10.0),
                    ),
                    Text(
                      'Exit app',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Are you sure to exit app?',
                      style: TextStyle(color: Colors.white, fontSize: 14.0),
                    ),
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.cancel,
                        color: Colors.black,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'CANCEL',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: ()=> exit(0),

                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.check_circle,
                        color: Colors.black,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'YES',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          );
        }
          )??
        false;
  }
  @override
  void initState() {
    super.initState();

    FirebaseAdMob.instance
        .initialize(appId: "ca-app-pub-9179231796573696~7800930750");
    _bannerAd = createBannerAd()
      ..load()
      ..show();
    getAnimalCategory();
    getAnimalSellingData();
    getHelpVideo();
    _getCurrentLocation();
    searchController.addListener(() {
      setState(() {
        filter = searchController.text;
      });
    });
  }
  InterstitialAd _interstitialAd;
  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
    _bannerAd?.dispose();
    _interstitialAd.dispose();
    searchController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
        drawer: Drawer(
          child:
          ListView(
              children:[
          Container(
              color: Colors.white,
              child: Column(children: [
                Container(
                  child: Stack(
                    children: <Widget>[
                      Center(
                        child: Container(
                          child: Image.asset(
                            'assets/images/logo-app.png',
                            width: 250,
                            height: 270,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: InkWell(
                    onTap: () async {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ViewAnimalCategory()));
                    },
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      color: Colors.white,
                      elevation: 5,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Image.asset('assets/images/sell.png'),
                            title: Text(translator.translate('Animal Sale'),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: "Righteous",
                                    fontSize: 20)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  child: InkWell(
                    onTap: () async {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ViewPersonalSellingList()));
                    },
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      color: Colors.white,
                      elevation: 5,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Image.asset('assets/images/list.png'),
                            title: Text(translator.translate('My Selling List'),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontFamily: "Righteous")),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  child: InkWell(
                    onTap: () async {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => UserProfile()));
                    },
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      color: Colors.white,
                      elevation: 5,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Image.asset('assets/images/user.png'),
                            title: Text(translator.translate('Profile'),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: "Righteous",
                                    fontSize: 20)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  child: InkWell(
                    onTap: () async {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Setting()));
                    },
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      color: Colors.white,
                      elevation: 5,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Image.asset('assets/images/setting.png'),
                            title: Text(translator.translate('Setting'),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: "Righteous",
                                    fontSize: 20)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  child: InkWell(
                    onTap: () async {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Help(videoUrl: helpingVideo,)));
                    },
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      color: Colors.white,
                      elevation: 5,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Image.asset('assets/images/question.png'),
                            title: Text(translator.translate('Help'),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontFamily: "Righteous")),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(30, 10, 30, 10),
                  child: InkWell(

                    onTap: () async {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(

                              content: Text(translator.translate
                                ('Are you sure you want to sign out?'),
                                style: TextStyle(
                                    fontFamily: 'Righteous', color: Colors.black),
                              ),
                              backgroundColor: Colors.lightGreen[100],
                              actions: [
                                FlatButton(
                                  child: Text(translator.translate('Yes')),
                                  onPressed: () async {
                                    _signOut();
                                    Navigator.of(context).pop();
                                  },
                                ),
                                FlatButton(
                                  child: Text(translator.translate('No')),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          });
                    },
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      color: Colors.white,
                      elevation: 5,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(
                              Icons.power_settings_new,
                              color: Colors.black,
                            ),
                            title: Text(translator.translate('Logout'),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: "Righteous",
                                    fontSize: 16)),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ]
              )
          ),
          ]
          )
        ),
        appBar: AppBar(
          backgroundColor: Colors.lightGreen[500],
          title: Text(
            translator.translate('HomePage'),
            style: TextStyle(
                fontSize: 18, fontFamily: 'Righteous', color: Colors.white),
          ),
          bottom: PreferredSize(
              child: Container(
                color: Colors.black87,
                height: 8.0,
              ),
              preferredSize: Size.fromHeight(8.0)),
        ),
        body: isLoading
            ? SpinKitDoubleBounce(
                color: Colors.greenAccent[700],
              )
            : Center(
                child: ListView(
                    children: dataCheck
                        ? <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              margin:
                                  const EdgeInsets.fromLTRB(100, 10, 10, 10),
                              child: Card(
                                child: TextField(
                                  controller: searchController,
                                  decoration: InputDecoration(
                                    hintText: "Search",
                                    prefixIcon: Icon(
                                      Icons.search,
                                      color: Colors.green,
                                    ),
                                    fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(0.0),
                                      borderSide: new BorderSide(),
                                    ),
                                    //fillColor: Colors.green
                                  ),
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16.0),
                                ),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.all(10),
                              child: GridView.builder(
                                  shrinkWrap: true,
                                  physics: BouncingScrollPhysics(),
                                  itemCount: animalName?.length ?? 0,
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2),
                                  itemBuilder: (context, index) {
                                    return filter == null || filter == ""
                                        ? InkWell(
                                            onTap: () {
                                              createInterstitialAd()
                                                ..load()
                                                ..show();
                                              Navigator.push(
                                                context,
                                                new MaterialPageRoute(
                                                    builder: (context) =>
                                                        ViewBuyAnimalList(
                                                          animalListName:
                                                              animalName[index],
                                                        )),
                                              );
                                            },
                                            child: Card(
                                              elevation: 8,
                                              semanticContainer: true,
                                              color: Colors.white,
                                              child: Container(
//                            width: 100.0,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.elliptical(
                                                              60.0, 60.0)),
//                                  color: Colors.indigo[200],
                                                ),
                                                child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: [
                                                      ClipRRect(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15.0),
                                                        child: Image.network(
                                                          animalImage[index]
                                                              .toString(),
                                                          height: 80.0,
                                                          width: 140.0,
                                                          fit: BoxFit.fill,
                                                        ),
                                                      ),
                                                      Text(
                                                        animalName[index]
                                                            .toString(),
                                                        style: TextStyle(
                                                            fontFamily:
                                                                "Righteous",
                                                            fontSize: 20),
                                                      ),
                                                    ]),
                                              ),
                                            ),
                                          )
                                        : '${animalName[index]}'
                                                .toLowerCase()
                                                .contains(filter.toLowerCase())
                                            ? InkWell(
                                                onTap: () {
                                                  createInterstitialAd()
                                                    ..load()
                                                    ..show();
                                                  Navigator.push(
                                                    context,
                                                    new MaterialPageRoute(
                                                        builder: (context) =>
                                                            ViewBuyAnimalList(
                                                              animalListName:
                                                                  animalName[
                                                                      index],
                                                            )),
                                                  );
                                                },
                                                child: Card(
                                                  elevation: 8,
                                                  semanticContainer: true,
                                                  color: Colors.white,
                                                  child: Container(
//                            width: 100.0,
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.elliptical(
                                                                  60.0, 60.0)),
//                                  color: Colors.indigo[200],
                                                    ),
                                                    child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceEvenly,
                                                        children: [
                                                          ClipRRect(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        15.0),
                                                            child:
                                                                Image.network(
                                                              animalImage[index]
                                                                  .toString(),
                                                              height: 80.0,
                                                              width: 140.0,
                                                              fit: BoxFit.fill,
                                                            ),
                                                          ),
                                                          Text(
                                                            animalName[index]
                                                                .toString(),
                                                            style: TextStyle(
                                                                fontFamily:
                                                                    "Righteous",
                                                                fontSize: 20),
                                                          ),
                                                        ]),
                                                  ),
                                                ),
                                              )
                                            : new Container();
                                  }),
                            ),
                            SizedBox(
                              height: 20,
                            )
                          ]
                        : <Widget>[
                            Container(
                                padding: EdgeInsets.fromLTRB(
                                    50.0, 100.0, 50.0, 50.0),
                                child: Center(
                                  child: Text(
                                    translator.translate("No data available"),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 25.0,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Righteous',
                                      //decoration: TextDecoration.none
                                    ),
                                  ),
                                ))
                          ]),
              )));
  }
}
