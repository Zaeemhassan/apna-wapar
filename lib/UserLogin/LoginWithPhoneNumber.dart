import 'package:android_intent/android_intent.dart';
import 'package:apna_beepar/UserSignUpPage/SignUpWithPhone.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'LoginOtp.dart';

class LoginPhone extends StatefulWidget {
  @override
  _LoginPhoneState createState() => _LoginPhoneState();
}

class _LoginPhoneState extends State<LoginPhone> {
  var userName;
  var phoneNumber;
  var userAddress;
  var allUserName = [];
  var allUserAddress = [];
  List allUserPhoneNumber = [];
  List allUserRating = [];
  List allUserCount = [];
  var id;
  List document = [];
  bool isLoading = false;
  final FirebaseAuth auth = FirebaseAuth.instance;
  bool dataCheck = false;


  Future getAllUsers() async {
    setState(() {
      isLoading = true;
    });
    Firestore.instance.collection("users").getDocuments().then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);

        var allUserDate = (result.data);
        allUserName.add(allUserDate["name"]);
        allUserAddress.add(allUserDate["Address"]);
        allUserPhoneNumber.add(allUserDate["PhoneNumber"]);
      });
      print("data $allUserPhoneNumber");
      setState(() {
        isLoading = false;
      });
    });
  }

  void checkUser() {}
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();

    getAllUsers();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            body: isLoading
                ? SpinKitDoubleBounce(
                    color: Colors.greenAccent[700],
                  )
                : Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Column(children: [
                            SizedBox(
                              height: 40,
                            ),
                            Container(
                              child: Stack(
                                children: <Widget>[
                                  Center(
                                    child: Container(
                                      child: Image.asset(
                                        'assets/images/logo-app.png',
                                        width: 300,
                                        height: 300,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 80,
                            ),
                            Container(
                              alignment: Alignment.topCenter,
                              margin: EdgeInsets.all(10),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  labelText:
                                      translator.translate("Phone Number"),
                                  hintText: 'Phone Number',
                                  prefix: Padding(
                                    padding: EdgeInsets.all(2),
                                    child: Text('+92'),
                                  ),
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderSide: new BorderSide(),
                                  ),
                                  //fillColor: Colors.green
                                ),
                                maxLength: 10,
                                onSaved: (String value) {
                                  phoneNumber = value;
                                },
                                validator: (String value) {
                                  if (value.isEmpty) {
                                    return translator
                                        .translate('Enter your phone number');
                                  } else if (allUserPhoneNumber.isEmpty) {
                                    return translator.translate(
                                        translator.translate('The user does not exist! First, create an account'));
                                  }

                                  return null;
                                },
                              ),
                            ),
                          ]),
                          Container(
                              height: 50.0,
                              width: 400,
                              padding:
                                  EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 0.0),
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(color: Colors.white)),
                                color: Colors.lightGreen[700],
                                onPressed: () async {
                                  _formKey.currentState.save();
                                  if(_formKey.currentState.validate()) {
                                    if (allUserPhoneNumber.contains("0" +
                                        phoneNumber)) {
                                      Navigator.push(
                                        context,
                                        new MaterialPageRoute(
                                            builder: (context) =>
                                                LoginOTPScreen(
                                                    phone: phoneNumber)),
                                      );
                                    }

                                    else {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              content: Text(
                                                translator.translate(
                                                    translator.translate('User does not exist! First create account')),
                                                style: TextStyle(
                                                    fontFamily: 'Righteous',
                                                    color: Colors.black),
                                              ),
                                              backgroundColor:
                                              Colors.white,
                                              actions: [
                                                FlatButton(
                                                  child: Text(
                                                      translator.translate(
                                                          'Ok')),
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                ),
                                              ],
                                            );
                                          });
                                    }
                                  }
                                },
                                child: Text(
                                  translator.translate("Verify OTP"),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Righteous'),
                                ),
                              )),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                              margin: const EdgeInsets.all(20),
                              child: Row(children: [
                                Expanded(
                                  flex: -3,
                                  child: Text(
                                    translator.translate("New to Apna Wapar? "),
                                    style: TextStyle(color: Colors.black45),
                                  ),
                                ),
                                Expanded(
                                  flex: 0,
                                  child: GestureDetector(
                                    child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          translator.translate(
                                              "Create a New Account"),
                                          style: TextStyle(
                                              color: Colors.lightGreen[700],
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Righteous'),
                                        )),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (_) => SignUpPhone()));
                                    },
                                  ),
                                )
                              ])),
                          SizedBox(height: 20),
                        ],
                      ),
                    ),
                  )));
  }
}
