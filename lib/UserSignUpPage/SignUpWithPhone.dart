import 'package:apna_beepar/UserLogin/LoginWithPhoneNumber.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'otp.dart';
import 'package:connectivity/connectivity.dart';

class SignUpPhone extends StatefulWidget {
  @override
  _SignUpPhoneState createState() => _SignUpPhoneState();
}

class _SignUpPhoneState extends State<SignUpPhone> {
  var userName;
  var userAddress;
  var allUserName = [];
  var allUserAddress = [];
  List allUserPhoneNumber = [];
  List allUserRating = [];
  List allUserCount = [];
  var id;
  List document = [];
  TextStyle defaultStyle =
  TextStyle(color: Colors.black, fontSize: 16.0, fontFamily: 'Righteous');
  bool isLoading = false;
  final FirebaseAuth auth = FirebaseAuth.instance;
  bool dataCheck = false;
  TextEditingController searchController = new TextEditingController();
  String filter;

  Future getAllUsers() async {
    setState(() {
      isLoading = true;
    });
    Firestore.instance.collection("users").getDocuments().then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        id = result.documentID;
        document.add(id);

        var allUserDate = (result.data);
        print("data ${allUserDate}");
        allUserName.add(allUserDate["name"]);
        allUserAddress.add(allUserDate["Address"]);
        allUserPhoneNumber.add(allUserDate["PhoneNumber"]);
        allUserRating.add((allUserDate["rating"]));
        allUserCount.add(allUserDate["count"]);
      });
    });
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getAllUsers();
  }

  TextEditingController _controller = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Column(children: [
              Container(
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: Container(
                        child: Image.asset(
                          'assets/images/logo-app.png',
                          width: 300,
                          height: 300,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.topCenter,
                margin: EdgeInsets.all(10),
                child: TextFormField(
                  inputFormatters: [new WhitelistingTextInputFormatter(RegExp("[a-zA-Z ]")),],
                  keyboardType: TextInputType.name,
                  decoration: InputDecoration(
                    labelText: translator.translate("Name"),
                    hintText: translator.translate("Name"),
                    prefixIcon: Icon(
                      Icons.title,
                      color: Colors.lightGreen[400],
                    ),
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(),
                    ),
                    //fillColor: Colors.green
                  ),
                  onSaved: (String value) {
                    userName = value;
                  },
                  validator: (String value) {
                    for (int i = 0; i < allUserName.length; i++) {
                      if (value == allUserName[i]) {
                        return (translator.translate(
                            "User Name already exists. Please use another name"));
                      }
                    }
                    if (value.isEmpty) {
                      return translator.translate('Enter your name');
                    }
                    return null;
                  },
                ),
              ),
              Container(
                alignment: Alignment.topCenter,
                margin: EdgeInsets.all(10),
                child: TextFormField(
                  inputFormatters: [new WhitelistingTextInputFormatter(RegExp("[a-zA-Z 0-9]")),],
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: translator.translate("Address"),
                    hintText: translator.translate("Address"),
                    prefixIcon: Icon(
                      Icons.home_outlined,
                      color: Colors.lightGreen[400],
                    ),
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(),
                    ),
                    //fillColor: Colors.green
                  ),
                  onSaved: (String value) {
                    userAddress = value;
                  },
                  validator: (String value) {
                    if (value.isEmpty) {
                      return translator.translate('Enter your address');
                    }
                    return null;
                  },
                ),
              ),
              Container(
                alignment: Alignment.topCenter,
                margin: EdgeInsets.all(10),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: translator.translate("Phone Number"),
                    hintText: 'Phone Number',
                    prefix: Padding(
                      padding: EdgeInsets.all(2),
                      child: Text('+92'),
                    ),
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(),
                    ),
                    //fillColor: Colors.green
                  ),
                  maxLength: 10,
                  controller: _controller,
                  validator: (String value) {
                    for (int i = 0; i < allUserPhoneNumber.length; i++) {
                      if ("0" + value == allUserPhoneNumber[i]) {
                        return (translator.translate("Phone Number already exists. Please use another number"));
                      }
                    }
                    if (value.isEmpty) {
                      return translator.translate('Enter your phone number');
                    }
                    return null;
                  },
                ),
              ),
            ]),
            Container(
                height: 50.0,
                width: 400,
                padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 0.0),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white)),
                  color: Colors.lightGreen[700],
                  onPressed: () async {
                    var connectivityResult =
                        await (Connectivity().checkConnectivity());
                    if (connectivityResult == ConnectivityResult.mobile ||
                        (connectivityResult == ConnectivityResult.wifi)) {
                      _formKey.currentState.save();
                      if (_formKey.currentState.validate()) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => OTPScreen(
                                _controller.text, userName, userAddress)));
                      }
                    }
                    else{
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              content:RichText(
                                text: TextSpan(
                                  style: defaultStyle,
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                        translator.translate('No internet connection !  '),
                                        style: TextStyle(
                                            fontWeight:
                                            FontWeight
                                                .bold,
                                            color: Colors.red,
                                            fontSize:
                                            16)),
                                    TextSpan(
                                        text:translator.translate("Please connect your mobile to the internet and restart your app"),
                                        style: TextStyle(

                                            color: Colors.black,
                                            fontSize:
                                            16)
                                    ),
                                  ],
                                ),
                              ),
                              backgroundColor: Colors.white,

                              actions: [
                                FlatButton(
                                  child: Text(
                                      translator.translate('Ok')
                                  ),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          }
                      );

                    }
                  },

                  child: Text(
                    translator.translate('CREATE A NEW ACCOUNT'),
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Righteous'),
                  ),
                )),
            SizedBox(
              height: 20,
            ),
            Container(
                margin: const EdgeInsets.all(20),
                child: Row(children: [
                  Expanded(
                    flex: -3,
                    child: Text(
                      translator.translate("Already have an account? "),
                      style: TextStyle(color: Colors.black45),
                    ),
                  ),
                  Expanded(
                    flex: 0,
                    child: GestureDetector(
                      child: Container(
                          alignment: Alignment.center,
                          child: Text(
                            translator.translate("Sign In"),
                            style: TextStyle(
                                color: Colors.lightGreen[600],
                                fontFamily: 'Righteous'),
                          )),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (_) => LoginPhone()));
                      },
                    ),
                  )
                ])),
            SizedBox(height: 10.0),
          ],
        ),
      ),
    ));
  }
}
