
import 'package:apna_beepar/UserHomePage/HomePage.dart';
import 'package:apna_beepar/UserLogin/LoginWithPhoneNumber.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:localize_and_translate/localize_and_translate.dart';

class OTPScreen extends StatefulWidget {
  final String phone;
  final String name;
  final String address;
  OTPScreen(this.phone,this.name,this.address);
  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  String _verificationCode;
  final TextEditingController _pinPutController = TextEditingController();
  final FocusNode _pinPutFocusNode = FocusNode();
  final BoxDecoration pinPutDecoration = BoxDecoration(
    color: Colors.green[100],
    borderRadius: BorderRadius.circular(10.0),
    border: Border.all(
      color: const Color.fromRGBO(126, 203, 224, 1),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[500],
        title: Text(
              translator.translate('Verify OTP'),
          style: TextStyle(
              fontSize: 18, fontFamily: 'Righteous', color: Colors.white),
        ),
        bottom: PreferredSize(
            child: Container(
              color: Colors.black87,
              height: 8.0,
            ),
            preferredSize: Size.fromHeight(8.0)),

      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 40),
            child: Center(
              child: Text(
                'Verify +92-${widget.phone}',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: PinPut(
              fieldsCount: 6,
              textStyle: const TextStyle(fontSize: 20.0, color: Colors.black),
              eachFieldWidth: 40.0,
              eachFieldHeight: 55.0,
              focusNode: _pinPutFocusNode,
              controller: _pinPutController,
              submittedFieldDecoration: pinPutDecoration,
              selectedFieldDecoration: pinPutDecoration,
              followingFieldDecoration: pinPutDecoration,
              pinAnimationType: PinAnimationType.fade,
              onSubmit: (pin) async {
                try {
                  AuthCredential authCreds = PhoneAuthProvider.getCredential(verificationId: _verificationCode,  smsCode: pin);
                  final FirebaseUser user = (await FirebaseAuth.instance.signInWithCredential(authCreds)).user;

                    if (user != null) {
                      await DatabaseService(uid: user.uid).updateUserData(widget.name,"0"+widget.phone,widget.address);

                      Navigator.pushReplacement(
                          context, MaterialPageRoute(builder: (BuildContext context) => UserHomePage()));
                    }

                } catch (e) {
                  FocusScope.of(context).unfocus();
                  _scaffoldkey.currentState
                      .showSnackBar(SnackBar(content: Text('invalid OTP')));
                }
              },
            ),
          )
        ],
      ),
    );
  }

  _verifyPhone() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: '+92${widget.phone}',
        verificationCompleted: (AuthCredential credential) async {
          final FirebaseUser user = (await FirebaseAuth.instance.signInWithCredential(credential)).user;
            if (user != null) {
              await DatabaseService(uid: user.uid).updateUserData(widget.name,"0"+widget.phone,widget.address);

              Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (BuildContext context) => UserHomePage()));
            }

        },
        verificationFailed: (AuthException error) =>
            print('error message is ${error.message}'),
        codeSent: (String verficationID, [int forceResendingToken]) {
          setState(() {
            _verificationCode = verficationID;
          });
        },

        codeAutoRetrievalTimeout: (String verificationID) {
          setState(() {
            _verificationCode = verificationID;
          });
        },
        timeout: Duration(seconds: 120));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _verifyPhone();
  }
}
  class DatabaseService {
    var userId;
    var document = [];
    var userProfileId = [];
    var profileId;

    Future getUserProfile() async {
      Firestore.instance
          .collection("users")
          .getDocuments()
          .then((querySnapshot) {
        querySnapshot.documents.forEach((result) {
          profileId = result.documentID;
          document.add(profileId);
          userProfileId.add(result.data['UserId']);
        });
      });
    }

    double rating = 0;
    int count = 0;
    final String uid;

    DatabaseService({ this.uid });

    // collection reference
    final CollectionReference users = Firestore.instance.collection('users');

    Future<void> updateUserData(String name, var phone, var address) async {
      getUserProfile();
      return await users.document(uid).setData({

        'name': name,
        "Address": address,
        "PhoneNumber": phone,
        "UserId": uid,
        "rating": rating,
        "count": count
      });
    }
  }
